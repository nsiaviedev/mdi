<?php
//gestion des users pour le menu
$admin = $this->ion_auth->is_admin();
// $group = array('members');
// $groups = $this->ion_auth->in_group('admin',false, true);
// var_dump($admin);
// var_dump($groups);
if ($admin == true) {
	# code...
	?>
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
				<aside class="app-sidebar">
					<div class="app-sidebar__user">
						<div class="dropdown user-pro-body">
							<div>
								<img src="<?php echo base_url().'assets'; ?>/images/faces/nsia.png" alt="user-img" class="avatar avatar-md brround">
							</div>
							<div class="user-info">
								<div class="mb-2">
								<a href="#" class="" data-toggle="" aria-haspopup="true" aria-expanded="false"> <span class="font-weight-semibold text-white"><?php echo $user->email//echo $user->first_name . '' . $user->last_name ; ?></span>  </a>
								<br><span class="text-gray"><?php echo $user->company ?></span>
								</div>
								<a hidden href="#" title="settings" class="user-button"><i class="fa fa-cogs"></i></a>
								<a href="<?php echo site_url().'/auth/profile'; ?>" title="profile" class="user-button"><i class="fa fa-user"></i></a>
								<a href="<?php echo site_url().'/auth/logout'; ?>" title="se déconnecter" class="user-button"><i class="fa fa-sign-out"></i></a>
							</div>
						</div>
					</div>
					
					<ul class="side-menu">
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-tachometer"></i><span class="side-menu__label">ACCEUIL </span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li><a class="slide-item" href="<?php echo site_url('Home/'); ?>">Index</a></li>
							</ul>
						</li>
						<li class="slide" hidden>
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-bandcamp"></i><span class="side-menu__label">UI Kit</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="alerts.html" class="slide-item">Alerts</a>
								</li>
								<li>
									<a href="buttons.html" class="slide-item">Buttons</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-file-text-o"></i><span class="side-menu__label">CHARGER</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('Mdidata/add_new'); ?>" class="slide-item">Individuel</a>
								</li>
								<li>
									<a href="<?php echo site_url('GroupeController/add_newg'); ?>" class="slide-item">Groupe</a>
								</li>
								<li hidden="">
									<a href="<?php echo site_url('RenteController/add_newr'); ?>" class="slide-item">Rente</a>
								</li>
								<li hidden="">
									<a href="<?php echo site_url('ChapChapController/add_chapchap'); ?>" class="slide-item">Chap Chap</a>
								</li>
								<li >
									<a href="<?php echo site_url('IdentiteController/add_id'); ?>" class="slide-item">Recherche par ID client</a>
								</li>
								<li >
									<a href="<?php echo site_url('PoliceController/show_quittance'); ?>" class="slide-item">Polices Non Quittancées</a>
								</li>
								<li >
									<a href="<?php echo site_url('RetentionController/show_retention'); ?>" class="slide-item">Retention Bancassurance</a>
								</li>

							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-file-text-o"></i><span class="side-menu__label">EXPORTER </span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('Mdidata/export_data'); ?>" class="slide-item">Exporter xlsx ,xls ou csv</a>
								</li>
								<li>
									<a href="<?php echo site_url('ChapChapController/search'); ?>" class="slide-item">Chap Chap par période</a>
								</li>
								<li>
									<a href="<?php echo site_url('PrelevementController/search'); ?>" class="slide-item">Point des encaissements par période</a>
								</li>
							</ul>
						</li>

						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-lock"></i><span class="side-menu__label">UTILISATEURS</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('Auth/'); ?>" class="slide-item">Liste des utilisateurs</a>
								</li>
								<li>
									<a href="<?php echo site_url('Auth/create_user'); ?>" class="slide-item">Nouvel utilisateur</a>
								</li>
								<li>
									<a href="<?php echo site_url('Auth/create_group'); ?>" class="slide-item">Nouveau groupe</a>
								</li>
								<li>
									<a href="<?php echo site_url('Auth/change_password'); ?>" class="slide-item" hidden>Modifier son mot de passe</a>
								</li>

							</ul>
						</li>

						<li class="slide" hidden="">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-lock"></i><span class="side-menu__label">Account</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="login.html" class="slide-item">Login</a>
								</li>
								<li>
									<a href="register.html" class="slide-item">Register</a>
								</li>
								<li>
									<a href="forgot-password.html" class="slide-item">Forgot password</a>
								</li>
								<li>
									<a href="lockscreen.html" class="slide-item">Lock screen</a>
								</li>

							</ul>
						</li>

					</ul>
				</aside>
	<?php
}
else{
	?>
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
				<aside class="app-sidebar">
					<div class="app-sidebar__user">
						<div class="dropdown user-pro-body">
							<div>
								<img src="<?php echo base_url().'assets'; ?>/images/faces/nsia.png" alt="user-img" class="avatar avatar-md brround">
							</div>
							<div class="user-info">
								<div class="mb-2">
								<a href="#" class="" data-toggle="" aria-haspopup="true" aria-expanded="false"> <span class="font-weight-semibold text-white"><?php echo $user->email//echo $user->first_name . '' . $user->last_name ; ?></span>  </a>
								<br><span class="text-gray"><?php echo $user->company ?></span>
								</div>
								<a hidden href="#" title="settings" class="user-button"><i class="fa fa-cogs"></i></a>
								<a href="<?php echo site_url().'/auth/profile'; ?>" title="profile" class="user-button"><i class="fa fa-user"></i></a>
								<a href="<?php echo site_url().'/auth/logout'; ?>" title="se déconnecter" class="user-button"><i class="fa fa-sign-out"></i></a>
							</div>
						</div>
					</div>
					
					<ul class="side-menu">
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-tachometer"></i><span class="side-menu__label">ACCEUIL </span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li><a class="slide-item" href="<?php echo site_url('Home/'); ?>">Index</a></li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-file-text-o"></i><span class="side-menu__label">CHARGER </span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('Mdidata/add_new'); ?>" class="slide-item">Individuels</a>
								</li>
								<li>
									<a href="<?php echo site_url('GroupeController/add_newg'); ?>" class="slide-item">Groupes</a>
								</li>
								<li hidden="">
									<a href="<?php echo site_url('RenteController/add_newr'); ?>" class="slide-item">Rente</a>
								</li>
								<li hidden="">
									<a href="<?php echo site_url('IdentiteController/add_iden'); ?>" class="slide-item">Chap Chap</a>
								</li>
								<li hidden="">
									<a href="<?php echo site_url('ChapChapController/add_chapchap'); ?>" class="slide-item">Chap Chap</a>
								</li>

							</ul>
						</li>

						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-lock"></i><span class="side-menu__label">EXPORTER </span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('Mdidata/export_data'); ?>" class="slide-item">Exporter xlsx ,xls ou csv</a>
								</li>
								<li>
									<a href="<?php echo site_url('ChapChapController/search'); ?>" class="slide-item">Chap Chap par période</a>
								</li>
							</ul>
						</li>

					</ul>
				</aside>
	<?php
}
?>
