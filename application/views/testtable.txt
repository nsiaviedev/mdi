<!-- test table -->

<div class="row">
<div class="col-md-12 col-lg-12">
<div class="card">
	<div class="card-header">
		<h3 class="card-title">Full color variations</h3> 	<a class="pull-right btn btn-primary btn-xs" href="<?php echo site_url()?>/Mdidata/createxls"><i class="fa fa-file-excel-o"></i> Export Data</a>
	</div>
	<div class="table-responsive">
		
		
		<?php
		
		//page manu
			$config['base_url'] = base_url().'Mdidata/export_data';        
			$config['total_rows'] = $this->mdi->get_count_sondage_querys();
			
			$config['per_page'] = 50;        
			$config['uri_segment'] = 3;        
			$config['use_page_numbers'] = TRUE;        
			$config['full_tag_open'] = '<ul class="pagination">';        
			$config['full_tag_close'] = '</ul>';  
			$config['attributes'] = array('class' => 'page-link');   
			$config['first_link'] = 'First';        
			$config['last_link'] = 'Last';   
			$config['first_tag_open'] = '<li>';        
			$config['first_tag_close'] = '</li>';        
			$config['prev_link'] = '&laquo';        
			$config['prev_tag_open'] = '<li class="prev">';        
			$config['prev_tag_close'] = '</li>';        
			$config['next_link'] = '&raquo';        
			$config['next_tag_open'] = '<li>';        
			$config['next_tag_close'] = '</li>';        
			$config['last_tag_open'] = '<li>';        
			$config['last_tag_close'] = '</li>';        
			$config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';        
			$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';        
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['num_links'] = 1;

			
			$this->pagination->initialize($config);   
			// $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			
			
		
			$page = $this->uri->segment(3);
			$start = ($page - 0) * $config["per_page"];
			$pagination = $this->pagination->create_links();     
			$tuples = $this->mdi->get_sondage_pagination_querys($config["per_page"], $start);
			
		//fin page
		
		if(!empty($tuples)) {
			$columns_names = array_keys($tuples[0]);
		 
			echo '<table class="table card-table table-vcenter text-nowrap table-primary" >
		<thead  class="bg-primary text-white">
			<tr>';
			foreach($columns_names as $col) {
				echo '<th class="text-white">'. $col .'</th>';
			}
			echo '</tr>
			</thead>
			<tbody>';
			foreach($tuples as $tuple) {
				echo '<tr>';
				foreach($tuple as $col) {
					echo '<td>'. $col .'</td>';
				}
				echo '</tr>';
			}
			}
		else {
			 echo '<tr colspan="5">
				<td>Aucune donnée...</td>
			</tr>';
		}
			echo '</tbody>
			</table>';
		
		?>
	
	<?php echo $pagination; ?>
	
	</div>
	<!-- table-responsive -->
</div>
</div>
</div>

<!-- test table -->