<!doctype html>
<html lang="fr" dir="ltr">
	
<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="msapplication-TileColor" content="#0061da">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />


		<!-- Title -->
		<title>NSIA - MISE A DISPOSITION D'INFORMATION</title>
		<link rel="stylesheet" href="<?php echo base_url().'assets'; ?>/fonts/fonts/font-awesome.min.css">

		<!-- Font Family -->
		<link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

		<!-- Sidemenu Css -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/toggle-sidebar/sidemenu.css" rel="stylesheet" />


		<!-- Dashboard Css -->
		<link href="<?php echo base_url().'assets'; ?>/css/dashboard.css" rel="stylesheet" />

		<!-- c3.js Charts Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

		<!-- select2 Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/select2/select2.min.css" rel="stylesheet" />

		<!-- Time picker Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/time-picker/jquery.timepicker.css" rel="stylesheet" />

		<!-- Date Picker Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/date-picker/spectrum.css" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="<?php echo base_url().'assets'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

		<!-- file Uploads -->
        <link href="<?php echo base_url().'assets'; ?>/plugins/fileuploads/css/dropify.css" rel="stylesheet" type="text/css" />
		<!---Font icons-->
		<link href="<?php echo base_url().'assets'; ?>/plugins/iconfonts/plugin.css" rel="stylesheet" />
		<script type="text/javascript">
		$(document).ready(function() {
			$("#frmCSVImport").on("submit", function () {

		 //   $("#response").attr("class", "");
			// 	$("#response").html("");
			// 	var fileType = ".";
			// 	var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
			// 	if (!regex.test($("#file").val().toLowerCase())) {
			// 	   $("#response").addClass("error");
			// 	   $("#response").addClass("display-block");
			// 		$("#response").html("Invalid File. Upload : <b>" + fileType + "</b> Files.");
			// 		return false;
			// 	}
			// 	return true;
			// });
		});
		</script>

		<script >

            function display_loader()
			{
			     document.getElementById('loader').innerHTML = "<div class='lox'><img class='' src='<?php echo base_url('assets/images/loader.svg'); ?>' alt='load'/>Veuillez patienté pendant le chargement du fichier...</div>";
			}
        </script>

		<style>
			.success {
				background: #c7efd9;
				border: #bbe2cd 1px solid;
			}

			.error {
				background: #fbcfcf;
				border: #f3c6c7 1px solid;
			}
			.test {
				//border: 1px solid red;
				position: relative;
				top: -8px
			}
			.lo{
				position: fixed;
				z-index: 9000;
				left: 0;
				top: 0;
				right: 0;
				bottom: 0;
				margin: 0 auto;
			}

			.lox{
				margin-left:500px;
			}
		</style>
	</head>

	<?php 
	    		if(!empty($oo)) {

	    			echo '<script>
					$(function testverifs(){
						console.log("tampe is ready to disable");
						//Begin
						$("#pppp").hide();
						$("#ppp2").hide();
						$(".okok").hide();
						$(".page-mains").hide();
							
					});
				</script>';

			
				    }
				
				?>
<body class="app sidebar-mini rtl">
<div id="/global-loader/" ></div>
<div class="page">
<div class="page-mains">
<?php include('template/header.php'); ?>

<!-- Sidebar menu-->
<?php include('template/nav.php'); ?>

<div class="app-content  my-3 my-md-5">
<div class="side-app">
<div class="page-header">
<h4 class="page-title">MDI</h4>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">MDI</a></li>
<li class="breadcrumb-item active" aria-current="page">Charger un fichier</li>
</ol>

</div>


	    		
	    		

<div class="row">
<div class="col-md-12">
<form class="card" action="<?php base_url('mdidata/add_new');  ?>" method="post" name="frmCSVImport" id="frmCSVImport" enctype="multipart/form-data">
<?php //echo form_open_multipart('mdidata/add_new');?>
	<div class="card-header okok">
		<h3 class="card-title">Charger un fichier individuel <?php  ?></h3>
	</div>

	<span id="success_message"></span>

	<div class="card-body">
		<div class="row">
			<div class="col-md-12 okok">
				<input type="file" name="file" id="file" accept="." class="dropify" data-height="110" />
				<span id="upload_error"></span>
			</div>
			

			<div class="col-md-6">
                     <?php 
						if(!empty($result_msg)) { echo '<span class="alert alert-success">'.$result_msg.'</span>'; }
					?>
				<div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>">
					<?php 
						if(!empty($message)) { echo $message; }
					?>
					<?php if($this->session->has_userdata('error_file')){ ?>
						<div class="alert alert-warning" role="alert">
						<?php echo $this->session->flashdata('error_file'); ?>
						</div>
					<?php } ?>
					<?php if($this->session->has_userdata('success')){ ?>
						<div class="alert alert-primary" role="alert">
						<?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php } ?>
					<?php if($this->session->has_userdata('error')){ ?>
						<div class="alert alert-danger" role="alert">
						<?php echo $this->session->flashdata('error'); ?>
						</div>
					<?php } ?>
				</div>
			</div>


		</div>
		
	</div>

	<div id="loader">
	</div>

	<div id="pppp" class="card-body">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="form-group m-0">
					<label class="form-label"></label>
					<div class="row gutters-xs">

						<!-- BEGIN SELECT -->
						<div class='col-md-4 col-lg-4'>
							<div class="col-auto">
								<label class="colorinput">
									<input id="shchp" name="JOINTABLES[]" type="checkbox"  value=" LEFT OUTER JOIN NSIACIF.JAPOLIP POLICE ON NUMERO_POLICE = POLICE.JAPOLIP_WNUPO " class="colorinput-input" />
									<span class="colorinput-color bg-azure"></span>
									<span class="custom-switch-description test">Je veux des informations sur la police</span>
								</label>
							</div>
							<div class="hidchp">
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchp1" name="JOINCHAMPS[]" type="checkbox"  value="POLICE.JAPOLIP_WNUPO AS CODES_POLICES " class="colorinput-input" />
										<span class="colorinput-color bg-azure"></span>
										<span class="custom-switch-description test">N° interne de la police</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchp2" name="JOINCHAMPS[]" type="checkbox"  value="POLICE.JAPOLIP_ANUPO AS NUMEROS_EXTERNES " class="colorinput-input" />
										<span class="colorinput-color bg-azure"></span>
										<span class="custom-switch-description test">N° externe de la police</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchp3" name="JOINCHAMPS[]" type="checkbox"  value="POLICE.JAPOLIP_DEFPO AS DATES_EFFETS" class="colorinput-input" />
										<span class="colorinput-color bg-azure"></span>
										<span class="custom-switch-description test">Date debut effet de la police</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchp4" name="JOINCHAMPS[]" type="checkbox"  value="POLICE.JAPOLIP_DFEPO AS FINS_EFFETS" class="colorinput-input" />
										<span class="colorinput-color bg-azure"></span>
										<span class="custom-switch-description test">Date fin effet de la police</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchp5" name="JOINCHAMPS[]" type="checkbox"  value="POLICE.JAPOLIP_MRGPO AS MODES_REGLEMENTS " class="colorinput-input" />
										<span class="colorinput-color bg-azure"></span>
										<span class="custom-switch-description test">Mode de règlement de la police</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchp6" name="JOINCHAMPS[]" type="checkbox"  value="dbo.statut_police(POLICE.JAPOLIP_WNUPO,''I'',1) AS STATUTS_POLICES " class="colorinput-input" />
										<span class="colorinput-color bg-azure"></span>
										<span class="custom-switch-description test">Statut de la police</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchp7" name="JOINCHAMPS[]" type="checkbox"  value="POLICE.JAPOLIP_WNPRO AS CODES_PRODUITS" class="colorinput-input" />
										<span class="colorinput-color bg-azure"></span>
										<span class="custom-switch-description test">Code du produit</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchp8" name="JOINCHAMPS[]" type="checkbox"  value="POLICE.JAPOLIP_LIBPR1 AS LIBELLES_PRODUITS " class="colorinput-input" />
										<span class="colorinput-color bg-azure"></span>
										<span class="custom-switch-description test">Libellé du produit</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchp9" name="JOINCHAMPS[]" type="checkbox"  value="POLICE.JAPOLIP_WUCLI AS CODES_CLIENTS" class="colorinput-input" />
										<span class="colorinput-color bg-azure"></span>
										<span class="custom-switch-description test">Code souscripteur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchp10" name="JOINCHAMPS[]" type="checkbox"  value="POLICE.JAPOLIP_WPAID AS CODES_PAYEURS " class="colorinput-input" />
										<span class="colorinput-color bg-azure"></span>
										<span class="custom-switch-description test">Code payeur</span>
									</label>
								</div>

								<div class="col-auto">
									<label class="colorinput">
										<input id="shchp11" name="JOINCHAMPS[]" type="checkbox"  value=" POLICE.JAPOLIP_JAASSUP_WNUAD AS CODES_ASSURES " class="colorinput-input" />
										<span class="colorinput-color bg-azure"></span>
										<span class="custom-switch-description test">Code Assuré</span>
									</label>
								</div>
							</div>
							
						</div>
						<div class="col-md-4 col-lg-4">
							<div class="col-auto">
								<label class="colorinput">
									<input id="shchq" name="JOINTABLES[]" type="checkbox" value=" LEFT OUTER JOIN NSIACIF.JAQUITP QUITT ON %numero% = %idbd% " class="colorinput-input" />
									<span class="colorinput-color bg-yellow"></span>
									<span class="custom-switch-description test">Je veux des informations sur la quittance</span>
								</label>
							</div>
							<div class="hidchq">
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq1" name="JOINCHAMPS[]" type="checkbox" value="QUITT.WNUCO AS CODES_QUITTANCES " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">N° de la quittance</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq2" name="JOINCHAMPS[]" type="checkbox" value="QUITT.MPYCO AS MONTANTS_QUITTANCES " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Montant de la quittance</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq3" name="JOINCHAMPS[]" type="checkbox" value="dbo.montant_prime(POLICE.JAPOLIP_WNUPO,''I'') AS MONTANTS_PERIODIQUES " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Montant prime périodique</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq4" name="JOINCHAMPS[]" type="checkbox" value="dbo.duree_police(POLICE.JAPOLIP_WNUPO,''I'') AS DUREES_PERIODIQUES" class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Durée paiement prime périodique</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq5" name="JOINCHAMPS[]" type="checkbox" value="QUITT.DDPCO AS EFFETS_QUITTANCES" class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Date debut effet de la quittance</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq6" name="JOINCHAMPS[]" type="checkbox" value="QUITT.DFECO AS FINS_EFFETS_QUITTANCES " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Date fin effet de la quittance</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq7" name="JOINCHAMPS[]" type="checkbox" value="QUITT.DDQCO AS DATES_QUITTANCES " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Date de la quittance</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq8" name="JOINCHAMPS[]" type="checkbox" value="QUITT.INENC AS INDEX_QUITTANCES " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Index de la quittance</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq9" name="JOINCHAMPS[]" type="checkbox" value="( SELECT COUNT(*) FROM NSIACIF.JAQUITP COMPTE WHERE COMPTE.WNUPO = NUMERO_POLICE AND COMPTE.INENC = ''S'' AND COMPTE.MPYCO > 0 AND COMPTE.SOLDCO = 0  ) AS NOMBRES_QUITTANCES_SOLDEES " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Nombre de quittances soldées </span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq10" name="JOINCHAMPS[]" type="checkbox" value="(SELECT TOP 1 MAX(DERNIERE.WNUCO) FROM NSIACIF.JAQUITP DERNIERE WHERE DERNIERE.WNUPO=NUMERO_POLICE AND DERNIERE.MPYCO>0 AND DERNIERE.INENC = ''S'') AS DERNIERES_QUITTANCES_SOLDEES " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Dernière quittance soldée </span>
									</label>
								</div>
	 							<div class="col-auto">
									<label class="colorinput">
										<input id="shchq11" name="JOINCHAMPS[]" type="checkbox" value="(SELECT TOP 1 MAX(DER.DDPCO) FROM NSIACIF.JAQUITP DER WHERE DER.WNUPO=NUMERO_POLICE AND DER.MPYCO>0 AND DER.INENC = ''S'') AS DEBUTS_PERIODES_DQS " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Début période dernière quittance soldée </span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq12" name="JOINCHAMPS[]" type="checkbox" value="(SELECT TOP 1 MAX(DERN.DFECO) FROM NSIACIF.JAQUITP DERN WHERE DERN.WNUPO=NUMERO_POLICE AND DERN.MPYCO>0 AND DERN.INENC = ''S'') AS FINS_PERIODES_DQS " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Fin période dernière quittance soldée </span>
									</label>
	 							</div>
	 							<div class="col-auto">
									<label class="colorinput">
										<input id="shchq13" name="JOINCHAMPS[]" type="checkbox" value="(SELECT TOP 1 MDQS.MPYCO FROM NSIACIF.JAQUITP MDQS WHERE MDQS.WNUCO=(SELECT  TOP 1 MAX(MDQSS.WNUCO) FROM NSIACIF.JAQUITP MDQSS WHERE MDQSS.WNUPO=NUMERO_POLICE AND MDQSS.MPYCO>0 AND MDQSS.INENC = ''S'')) AS MONTANTS_DQS " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Montant DQS</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq14" name="JOINCHAMPS[]" type="checkbox" value="(SELECT TOP 1 VV.INENC FROM NSIACIF.JAQUITP VV WHERE VV.WNUCO = (SELECT TOP 1  MAX(XX.WNUCO) FROM NSIACIF.JAQUITP XX WHERE XX.MPYCO > 0 AND XX.WNUPO = NUMERO_POLICE AND XX.INENC = ''S'')) AS INDEX_DQS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Index dernière quittance soldée  </span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq15" name="JOINCHAMPS[]" type="checkbox" value="(SELECT TOP 1 NDQE.WNUCO FROM NSIACIF.JAQUITP NDQE WHERE NDQE.DDPCO = (SELECT TOP 1  MAX(NDQEE.DDPCO) FROM NSIACIF.JAQUITP NDQEE WHERE NUMERO_POLICE = NDQEE.WNUPO AND NDQEE.MPYCO>0)) AS NUMEROS_DQEM " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Numéro DQE</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq16" name="JOINCHAMPS[]" type="checkbox" value="(SELECT TOP 1 DPDQE.DDPCO FROM NSIACIF.JAQUITP DPDQE WHERE DPDQE.DDPCO = (SELECT  TOP 1  MAX(DPDQEE.DDPCO) FROM NSIACIF.JAQUITP DPDQEE WHERE NUMERO_POLICE = DPDQEE.WNUPO AND DPDQEE.MPYCO>0)) AS DEBUTS_DQEM " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Début période couverture DQE</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq17" name="JOINCHAMPS[]" type="checkbox" value="(SELECT TOP 1 FPDQE.DFECO FROM NSIACIF.JAQUITP FPDQE WHERE FPDQE.DDPCO=(SELECT  TOP 1 MAX(FPDQEE.DDPCO) FROM NSIACIF.JAQUITP FPDQEE WHERE FPDQEE.WNUPO=NUMERO_POLICE AND FPDQEE.MPYCO>0)) AS FINS_DQEM" class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Fin période couverture DQE </span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq18" name="JOINCHAMPS[]" type="checkbox" value="(SELECT TOP 1 MDQE.MPYCO FROM NSIACIF.JAQUITP MDQE WHERE MDQE.DDPCO=(SELECT  TOP 1 MAX(MDQEE.DDPCO) FROM NSIACIF.JAQUITP MDQEE WHERE MDQEE.WNUPO=NUMERO_POLICE AND MDQEE.MPYCO>0)) AS MONTANTS_DQEM " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Montant DQE</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchq19" name="JOINCHAMPS[]" type="checkbox" value="(SELECT TOP 1 MAX (CAST(JBTARTP_DPQRT AS VARCHAR)) FROM NSIACIF.JATARTP WHERE JBTARTP_WNUPO = POLICE.JAPOLIP_WNUPO) AS DATES_PROCHAINES_QUITTANCES " class="colorinput-input" />
										<span class="colorinput-color bg-yellow"></span>
										<span class="custom-switch-description test">Date prochaine de la quittance </span>
									</label>
								</div>
							</div>
							
						</div>
						<div class="col-md-4 col-lg-4">
							<div class="col-auto">
								<label class="colorinput">
									<input id="shchi" name="JOINTABLES[]" type="checkbox" value="LEFT OUTER JOIN NSIACIF.JAIDENP CLIENT ON POLICE.JAPOLIP_WUCLI = CLIENT.JAIDENP_WNUAD " class="colorinput-input" />
									<span class="colorinput-color bg-indigo"></span>
									<span class="custom-switch-description test">Je veux des informations sur le souscripteur</span>
								</label>
							</div>
							<div class="hidchi">
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchi1" name="JOINCHAMPS[]" type="checkbox" value="CLIENT.JAIDENP_NOMTOT AS NOM_CLIENT " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Nom total du souscripteur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchi2" name="JOINCHAMPS[]" type="checkbox" value="dbo.email_id(CLIENT.JAIDENP_WNUAD) AS EMAIL_CLIENT " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Adresse email du souscripteur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchi3" name="JOINCHAMPS[]" type="checkbox" value="dbo.adresse_id(CLIENT.JAIDENP_WNUAD) AS ADRESSE_CLIENT " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Adresse postale du souscripteur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchi4" name="JOINCHAMPS[]" type="checkbox" value="dbo.contact_id(CLIENT.JAIDENP_WNUAD) AS CONTACT_CLIENT " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Contacts du souscripteur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchi5" name="JOINCHAMPS[]" type="checkbox" value="CLIENT.JAIDENP_DNAAD AS DATE_NAISS_CLIENT " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Date de naissance du souscripteur</span>
									</label>
								</div>
							</div>
							
						</div>

						<!-- END SELECT -->

					</div>
				</div>
			</div>
		</div>
		
	</div>

	<div id="ppp2" class="card-body">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="form-group m-0">
					<label class="form-label"></label>
					<div class="row gutters-xs">

						<!-- BEGIN SELECT -->
						<div class='col-md-4 col-lg-4'> 
							<div class="col-auto">
								<label class="colorinput">
									<input id="shchpay" name="JOINTABLES[]" type="checkbox" value=" LEFT OUTER JOIN NSIACIF.JAIDENP PAYEUR ON POLICE.JAPOLIP_WUCLI = PAYEUR.JAIDENP_WNUAD " class="colorinput-input" />
									<span class="colorinput-color bg-indigo"></span>
									<span class="custom-switch-description test">Je veux des informations sur le payeur</span>
								</label>
							</div>
							<div class="hidchpay">
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchpay1" name="JOINCHAMPS[]" type="checkbox" value="PAYEUR.JAIDENP_NOMTOT AS NOMS_PAYEURS" class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Nom du payeur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchpay2" name="JOINCHAMPS[]" type="checkbox" value="dbo.email_id(PAYEUR.JAIDENP_WNUAD) AS EMAILS_PAYEURS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Adresse email du payeur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchpay3" name="JOINCHAMPS[]" type="checkbox" value="dbo.adresse_id(PAYEUR.JAIDENP_WNUAD) AS ADRESSES_PAYEURS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Adresse postale du payeur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchpay4" name="JOINCHAMPS[]" type="checkbox" value="dbo.contact_id(PAYEUR.JAIDENP_WNUAD) AS CONTACTS_PAYEURS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Contacts du payeur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchpay5" name="JOINCHAMPS[]" type="checkbox" value="PAYEUR.JAIDENP_DNAAD AS DATES_NAISS_PAYEURS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Date de naissance du payeur</span>
									</label>
								</div>
							</div>
							
						</div>

						<div class='col-md-4 col-lg-4'>
							<div class="col-auto">
								<label class="colorinput">
									<input id="shcha" name="JOINTABLES[]" type="checkbox" value=" LEFT OUTER JOIN NSIACIF.JAAGENP AGENT ON POLICE.JAPOLIP_JASCCDP_WINAG = AGENT.JAAGENP_WINAG " class="colorinput-input" />
									<span class="colorinput-color bg-indigo"></span>
									<span class="custom-switch-description test">Je veux des informations sur l'apporteur</span>
								</label>
							</div>
							<div class="hidcha">
								<div class="col-auto">
									<label class="colorinput">
										<input id="shcha1" name="JOINCHAMPS[]" type="checkbox" value="AGENT.JAAGENP_WINAG AS NUMEROS_APPORTEURS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Code de l'apporteur </span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shcha2" name="JOINCHAMPS[]" type="checkbox" value=" AGENT.JAAGENP_NOMTOT AS NOMS_APPORTEURS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Nom apporteur</span>
									</label>
								</div>
								
								<div class="col-auto">
									<label class="colorinput">
										<input id="shcha3" name="JOINCHAMPS[]" type="checkbox" value=" AGENT.JAAGENP_DEFAG AS EFFETS_APPORTEURS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Date effet de l'apporteur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shcha4" name="JOINCHAMPS[]" type="checkbox" value=" AGENT.JAAGENP_DFEAG AS FINS_EFFETS_APPORTEURS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Date fin effet de l'apporteur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shcha5" name="JOINCHAMPS[]" type="checkbox" value=" dbo.contact_id(AGENT.JAAGENP_WINAG) AS CONTACTS_APPORTEURS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Contact de l'apporteur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shcha6" name="JOINCHAMPS[]" type="checkbox" value="dbo.email_id(AGENT.JAAGENP_WINAG) AS EMAILS_APPORTEURS" class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Email de l'apporteur</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shcha7" name="JOINCHAMPS[]" type="checkbox" value=" AGENT.JAAGENP_QUAAG AS QUALIFICATION_APPORTEURS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Qualification de l'apporteur</span>
									</label>
								</div>
							<!-- -</div> -->
								<div class="col-auto">
									<label class="colorinput">
										<input id="shcha8" name="JOINCHAMPS[]" type="checkbox" value=" AGENT.JAAGENP_WINIT AS STRUCTURES_APPORTEURS " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Structure de l'apporteur</span>
									</label>
								</div>
							</div>
							
						</div>

						<div class='col-md-4 col-lg-4'>
							<div class="col-auto">
								<label class="colorinput">
									<input id="shchcompte" name="JOINTABLES[]" type="checkbox" value=" LEFT OUTER JOIN NSIACIF.JACORBP BANCAIRE ON POLICE.JAPOLIP_WNUPO = BANCAIRE.JACORBP_WNUPO " class="colorinput-input" />
									<span class="colorinput-color bg-indigo"></span>
									<span class="custom-switch-description test">Je veux des informations sur le compte bancaire</span>
								</label>
							</div>
							<div class="hidchcompte">
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchcompte1" name="JOINCHAMPS[]" type="checkbox" value="( CASE
								  WHEN ISNULL(BANCAIRE.JACORBP_BANXD,''NULL'')=''NULL'' THEN (SELECT TOP 1 JACORBP_BANXD FROM NSIACIF.JACORBP WHERE JACORBP_WNUAD = POLICE.JAPOLIP_WPAID AND JACORBP_USRNCB=''*'' )     
							   ELSE  BANCAIRE.JACORBP_BANXD 
							   END) AS NUMEROS_COMPTES_BANCAIRES " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Numéro de compte bancaire</span>
									</label>
								</div>
							</div>
							
						</div>





						<!-- <div class='col-md-4 col-lg-4'>
							<div class="col-auto">
								<label class="colorinput">
									<input id="shchco" name="JOINTABLES[]" type="checkbox" value="LEFT OUTER JOIN LINK_SUNSHINE.SUN_COTEDIVOIRE.NSIACIF.JAAGENP ON NUMERO_AGENT = JAAGENP_WINAG " class="colorinput-input" />
									<span class="colorinput-color bg-indigo"></span>
									<span class="custom-switch-description test">Je veux des informations sur le conseiller</span>
								</label>
							</div>
							<div class="hidchco">
								<div class="col-auto">
									<label class="colorinput">
										<input name="JOINCHAMPS[]" type="checkbox" value="(SELECT PAYEUR.JAIDENP_WNUAD FROM NSIACIF.JAIDENP PAYEUR WHERE B.JAPOLIP_WPAID=PAYEUR.JAIDENP_WNUAD) NUMERO_PAYEUR" class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Code du conseiller</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input name="JOINCHAMPS[]" type="checkbox" value="(SELECT PAYEUR.JAIDENP_WNUAD FROM NSIACIF.JAIDENP PAYEUR WHERE B.JAPOLIP_WPAID=PAYEUR.JAIDENP_WNUAD) NUMERO_PAYEUR" class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Nom du conseiller</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input name="JOINCHAMPS[]" type="checkbox" value="(SELECT PAYEUR.JAIDENP_WNUAD FROM NSIACIF.JAIDENP PAYEUR WHERE B.JAPOLIP_WPAID=PAYEUR.JAIDENP_WNUAD) NUMERO_PAYEUR" class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Prénom du conseiller</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input name="JOINCHAMPS[]" type="checkbox" value="JAAGENP_NOMTOT" class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Nom total du conseiller</span>
									</label>
								</div>
							</div>
							
						</div> -->

						<!-- END SELECT -->

					</div>
				</div>
			</div>
		</div>
		
	</div>

	<div id="ppp3" class="card-body">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="form-group m-0">
					<label class="form-label"></label>
					<div class="row gutters-xs">

						<!-- BEGIN SELECT -->
						<div class='col-md-4 col-lg-4'> 
							<div class="col-auto">
								<label class="colorinput">
									<input id="shchassu" name="JOINTABLES[]" type="checkbox" value=" LEFT OUTER JOIN NSIACIF.JAIDENP ASSURES ON POLICE.JAPOLIP_JAASSUP_WNUAD = ASSURES.JAIDENP_WNUAD " class="colorinput-input" />
									<span class="colorinput-color bg-indigo"></span>
									<span class="custom-switch-description test">Je veux des informations sur l'assuré</span>
								</label>
							</div>
							<div class="hidchassu">
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchassu1" name="JOINCHAMPS[]" type="checkbox" value="ASSURES.JAIDENP_NOMTOT AS NOMS_ASSURES" class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Nom de l'assuré</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchassu2" name="JOINCHAMPS[]" type="checkbox" value="dbo.email_id(ASSURES.JAIDENP_WNUAD) AS EMAILS_ASSURES " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Adresse email de l'assuré</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchassu3" name="JOINCHAMPS[]" type="checkbox" value="dbo.adresse_id(ASSURES.JAIDENP_WNUAD) AS ADRESSES_ASSURES " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Adresse postale de l'assuré</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchassu4" name="JOINCHAMPS[]" type="checkbox" value="dbo.contact_id(ASSURES.JAIDENP_WNUAD) AS CONTACTS_ASSURES " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Contacts de l'assuré</span>
									</label>
								</div>
								<div class="col-auto">
									<label class="colorinput">
										<input id="shchassu5" name="JOINCHAMPS[]" type="checkbox" value="ASSURES.JAIDENP_DNAAD AS DATES_NAISS_ASSURES " class="colorinput-input" />
										<span class="colorinput-color bg-indigo"></span>
										<span class="custom-switch-description test">Date de naissance de l'assuré</span>
									</label>
								</div>
							</div>
							
						</div>

					</div>
				</div>
			</div>
		</div>
		
	</div>


	
	<div class="card-body">
		<div class="form-footer">
			<button type="submit" onclick="display_loader()" id="submit" name="import" class="btn btn-primary btn-block">Charger</button>
		</div>
	</div>
</form>

<div class="form-group" id="process" style="display: none;">
		<div class="progress">
			<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valumax="100"></div>
		</div>
	</div>

						
</div>

</div>


</div>
</div>
</div>

			

			<!--footer-->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
							Copyright © <?php echo date('Y'); ?> <a href="#">NSIA VIE & ASSURANCE</a>. All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->
		</div>

		<!-- Back to top -->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>
		<!-- Dashboard Css -->
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/bootstrap.bundle.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/jquery.sparkline.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/selectize.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/jquery.tablesorter.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/circle-progress.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/rating/jquery.rating-stars.js"></script>

		<!-- Fullside-menu Js-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/toggle-sidebar/sidemenu.js"></script>


		<!--Select2 js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/select2/select2.full.min.js"></script>

		<!-- Timepicker js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/time-picker/jquery.timepicker.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/time-picker/toggles.min.js"></script>

		<!-- Datepicker js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/date-picker/spectrum.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/date-picker/jquery-ui.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/input-mask/jquery.maskedinput.js"></script>

		<!-- Inline js -->
		<script src="<?php echo base_url().'assets'; ?>/js/select2.js"></script>
		<!-- file uploads js -->
        <script src="<?php echo base_url().'assets'; ?>/plugins/fileuploads/js/dropify.js"></script>

		<!-- Custom scroll bar Js-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- Custom Js-->
		<script src="<?php echo base_url().'assets'; ?>/js/custom.js"></script>

		<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script> -->

		<script >
            $('.dropify').dropify({
                messages: {
                    'default': 'Glisser pour deposer votre fichier ou cliquez pour charger',
                    'replace': 'Glisser pour deposer votre fichier ou cliquez pour remplacer',
                    'remove': 'Supprimer',
                    'error': 'Ooops, something wrong appended.'
                },
                error: {
                    'fileSize': 'The file size is too big (2M max).'
                }
            });
        </script>

        <script>
			$(function(e) {
				$('#example').DataTable({
					dom: 'Bfrtip',
		        buttons: [
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdfHtml5'
		        ]
				});
			} );
		</script>

		<script>
		$(function testverif(){
			console.log('is ready to disable');
			//Begin
			$(".hidchp").hide();
			$(".hidchq").hide();
			$(".hidchi").hide();
			$(".hidcha").hide();
			$(".hidchco").hide();
			$(".hidchpay").hide();
			$(".hidchcompte").hide();
			$(".hidchassu").hide();
			
			//Second option to display
			$('#shchp').on('change',function(){
				if(document.getElementById("shchp").checked == true){
					console.log('is ready to enable');
					$(".hidchp").show();
				}else{
					console.log('is ready to disable');
					$(".hidchp").hide();
					document.getElementById("shchp1").checked = false;
					document.getElementById("shchp2").checked = false;
					document.getElementById("shchp3").checked = false;
					document.getElementById("shchp4").checked = false;
					document.getElementById("shchp5").checked = false;
					document.getElementById("shchp6").checked = false;
					document.getElementById("shchp7").checked = false;
					document.getElementById("shchp8").checked = false;
					document.getElementById("shchp9").checked = false;
					document.getElementById("shchp10").checked = false;
					document.getElementById("shchp11").checked = false;
				}
			});

			//Second option to display
			$('#shchq').on('change',function(){
				if(document.getElementById("shchq").checked == true){
					console.log('is ready to enable');
					$(".hidchq").show();
				}else{
					console.log('is ready to disable');
					$(".hidchq").hide();
					document.getElementById("shchq1").checked = false;
					document.getElementById("shchq2").checked = false;
					document.getElementById("shchq3").checked = false;
					document.getElementById("shchq4").checked = false;
					document.getElementById("shchq5").checked = false;
					document.getElementById("shchq6").checked = false;
					document.getElementById("shchq7").checked = false;
					document.getElementById("shchq8").checked = false;
					document.getElementById("shchq9").checked = false;
					document.getElementById("shchq10").checked = false;
					document.getElementById("shchq11").checked = false;
					document.getElementById("shchq12").checked = false;
					document.getElementById("shchq13").checked = false;
					document.getElementById("shchq14").checked = false;
					document.getElementById("shchq15").checked = false;
					document.getElementById("shchq16").checked = false;
					document.getElementById("shchq17").checked = false;
					document.getElementById("shchq18").checked = false;
					document.getElementById("shchq19").checked = false;
				}
			});

			//Second option to display
			$('#shchi').on('change',function(){
				if(document.getElementById("shchi").checked == true){
					console.log('is ready to enable');
					$(".hidchi").show();
				}else{
					console.log('is ready to disable');
					$(".hidchi").hide();
					document.getElementById("shchi1").checked = false;
					document.getElementById("shchi2").checked = false;
					document.getElementById("shchi3").checked = false;
					document.getElementById("shchi4").checked = false;
					document.getElementById("shchi5").checked = false;
				}
			});

			//Second option to display
			$('#shcha').on('change',function(){
				if(document.getElementById("shcha").checked == true){
					console.log('is ready to enable');
					$(".hidcha").show();
				}else{
					console.log('is ready to disable');
					$(".hidcha").hide();
					document.getElementById("shcha1").checked = false;
					document.getElementById("shcha2").checked = false;
					document.getElementById("shcha3").checked = false;
					document.getElementById("shcha4").checked = false;
					document.getElementById("shcha5").checked = false;
					document.getElementById("shcha6").checked = false;
					document.getElementById("shcha7").checked = false;
					document.getElementById("shcha8").checked = false;
				}
			});

			//Second option to display
			$('#shchpay').on('change',function(){
				if(document.getElementById("shchpay").checked == true){
					console.log('is ready to enable');
					$(".hidchpay").show();
				}else{
					console.log('is ready to disable');
					$(".hidchpay").hide();
					document.getElementById("shchpay1").checked = false;
					document.getElementById("shchpay2").checked = false;
					document.getElementById("shchpay3").checked = false;
					document.getElementById("shchpay4").checked = false;
					document.getElementById("shchpay5").checked = false;
				}
			});

			//Second option to display
			$('#shchco').on('change',function(){
				if(document.getElementById("shchco").checked == true){
					console.log('is ready to enable');
					$(".hidchco").show();
				}else{
					console.log('is ready to disable');
					$(".hidchco").hide();
					//document.getElementById("shchco").checked = false;
				}
			});

			//Second option to display
			$('#shchcompte').on('change',function(){
				if(document.getElementById("shchcompte").checked == true){
					console.log('is ready to enable');
					$(".hidchcompte").show();
				}else{
					console.log('is ready to disable');
					$(".hidchcompte").hide();
					document.getElementById("shchcompte1").checked = false;
				}
			});

			//Second option to display
			$('#shchassu').on('change',function(){
				if(document.getElementById("shchassu").checked == true){
					console.log('is ready to enable');
					$(".hidchassu").show();
				}else{
					console.log('is ready to disable');
					$(".hidchassu").hide();
					document.getElementById("shchassu1").checked = false;
					document.getElementById("shchassu2").checked = false;
					document.getElementById("shchassu3").checked = false;
					document.getElementById("shchassu4").checked = false;
					document.getElementById("shchassu5").checked = false;
				}
			});
				
		});
	</script>
        

	</body>
</html>


