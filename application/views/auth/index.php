<!doctype html>
<html lang="en" dir="ltr">
	
<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="msapplication-TileColor" content="#0670f0">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

		<!-- Title -->
		<title>NSIA - MISE A DISPOSITION D'INFORMATION</title>
		<link rel="stylesheet" href="<?php echo base_url().'assets'; ?>/fonts/fonts/font-awesome.min.css">

		<!-- Font Family -->
		<link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

		<!-- Sidemenu Css -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/toggle-sidebar/sidemenu.css" rel="stylesheet" />


		<!-- Dashboard Css -->
		<link href="<?php echo base_url().'assets'; ?>/css/dashboard.css" rel="stylesheet" />

		<!-- c3.js Charts Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

		<!-- Data table css -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />

		<!-- Slect2 css -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/select2/select2.min.css" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="<?php echo base_url().'assets'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

		<!---Font icons-->
		<link href="<?php echo base_url().'assets'; ?>/plugins/iconfonts/plugin.css" rel="stylesheet" />
		
		<!-- Icone NSIA-->
		<link href="<?php echo base_url('assets/images/fav.ico');?>" rel="icon">
	
	</head>
	<body class="app sidebar-mini rtl">
		<div id="#global-loader" ></div>
		<div class="page">
			<div class="page-main">
				<div class="app-header header py-1 d-flex">
					<div class="container-fluid">
						<div class="d-flex">
							<a class="header-brand" href="index">
								<img src="<?php echo base_url().'assets'; ?>/images/brand/nsia-logo.png" class="header-brand-img" alt="Plight logo">
							</a>
							<a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a>
							<div class="mt-2">
								<div class="searching mt-3 ml-2">
									<a href="javascript:void(0)" class="search-open mt-3">
										<i class="fa fa-search text-white"></i>
									</a>
									<div class="search-inline">
										<form>
											<input type="text" class="form-control" placeholder="Search here">
											<button type="submit">
												<i class="fa fa-search"></i>
											</button>
											<a href="javascript:void(0)" class="search-close">
												<i class="fa fa-times"></i>
											</a>
										</form>
									</div>
								</div>
							</div>
							<div class="d-flex order-lg-2 ml-auto">
								<div class="dropdown d-none d-md-flex mt-1" >
									<a  class="nav-link icon full-screen-link">
										<i class="fe fe-maximize floating"  id="fullscreen-button"></i>
									</a>
								</div>
							
								<div class="dropdown mt-1">
									<a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
										<span class="avatar avatar-md brround" style="background-image: url(<?php echo base_url().'assets/'; ?>/images/faces/nsia.png)"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow ">
										<div class="text-center">
											<a href="#" class="dropdown-item text-center font-weight-sembold user"><?php echo $user->email //echo $user->first_name . '' . $user->last_name ; ?></a>
											<span class="text-center user-semi-title text-dark"><?php echo $user->company ?></span>
											<div class="dropdown-divider"></div>
										</div>
										<a class="dropdown-item" href="#">
											<i class="dropdown-icon mdi mdi-account-outline "></i> Profile
										</a>
										<a class="dropdown-item" href="<?php echo site_url().'/auth/logout'; ?>">
											<i class="dropdown-icon mdi  mdi-logout-variant"></i> Se déconnecter
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Sidebar menu-->
				<?php
//gestion des users pour le menu
$admin = $this->ion_auth->is_admin();
// $group = array('members');
// $groups = $this->ion_auth->in_group('admin',false, true);
// var_dump($admin);
// var_dump($groups);
if ($admin == true) {
	# code...
	?>
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
				<aside class="app-sidebar">
					<div class="app-sidebar__user">
						<div class="dropdown user-pro-body">
							<div>
								<img src="<?php echo base_url().'assets'; ?>/images/faces/nsia.png" alt="user-img" class="avatar avatar-md brround">
							</div>
							<div class="user-info">
								<div class="mb-2">
								<a href="#" class="" data-toggle="" aria-haspopup="true" aria-expanded="false"> <span class="font-weight-semibold text-white"><?php echo $user->email//echo $user->first_name . '' . $user->last_name ; ?></span>  </a>
								<br><span class="text-gray"><?php echo $user->company ?></span>
								</div>
								<a hidden href="#" title="settings" class="user-button"><i class="fa fa-cogs"></i></a>
								<a href="<?php echo site_url().'/auth/profile'; ?>" title="profile" class="user-button"><i class="fa fa-user"></i></a>
								<a href="<?php echo site_url().'/auth/logout'; ?>" title="se déconnecter" class="user-button"><i class="fa fa-sign-out"></i></a>
							</div>
						</div>
					</div>
					
					<ul class="side-menu">

						<li class="slide">
			              <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-tachometer"></i><span class="side-menu__label">ACCEUIL </span><i class="angle fa fa-angle-right"></i></a>
			              <ul class="slide-menu">
			                <li><a class="slide-item" href="<?php echo site_url('Home/'); ?>">Index</a></li>
			              </ul>
			            </li>

						<li class="slide" hidden="">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-tachometer"></i><span class="side-menu__label">Home</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li><a class="slide-item" href="<?php echo site_url('Home/'); ?>">Dashboard 01</a></li>
							</ul>
						</li>
						<li class="slide" hidden>
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-bandcamp"></i><span class="side-menu__label">UI Kit</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="alerts.html" class="slide-item">Alerts</a>
								</li>
								<li>
									<a href="buttons.html" class="slide-item">Buttons</a>
								</li>
							</ul>
						</li>

						<li class="slide">
			              <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-file-text-o"></i><span class="side-menu__label">CHARGER </span><i class="angle fa fa-angle-right"></i></a>
			              <ul class="slide-menu">
			                <li>
			                  <a href="<?php echo site_url('Mdidata/add_new'); ?>" class="slide-item">Individuel</a>
			                </li>
			                <li>
			                  <a href="<?php echo site_url('GroupeController/add_newg'); ?>" class="slide-item">Groupe</a>
			                </li>
			                <li>
			                  <a href="<?php echo site_url('RenteController/add_newr'); ?>" class="slide-item">Rente</a>
			                </li>
			              </ul>
			            </li>

			            <li class="slide">
			              <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-file-text-o"></i><span class="side-menu__label">EXPORTER </span><i class="angle fa fa-angle-right"></i></a>
			              <ul class="slide-menu">
			                <li>
			                  <a href="<?php echo site_url('Mdidata/export_data'); ?>" class="slide-item">Exporter xlsx ,xls ou csv</a>
			                </li>
			                <li hidden="">
			                  <a href="<?php echo site_url('Mdidata/test'); ?>" class="slide-item">Exporter les données</a>
			                </li>
			              </ul>
			            </li>

						<li class="slide">
			              <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-lock"></i><span class="side-menu__label">UTILISATEURS</span><i class="angle fa fa-angle-right"></i></a>
			              <ul class="slide-menu">
			                <li>
			                  <a href="<?php echo site_url('Auth/'); ?>" class="slide-item">Liste des utilisateurs</a>
			                </li>
			                <li>
			                  <a href="<?php echo site_url('Auth/create_user'); ?>" class="slide-item">Nouvel utilisateur</a>
			                </li>
			                <li>
			                  <a href="<?php echo site_url('Auth/create_group'); ?>" class="slide-item">Nouveau groupe</a>
			                </li>
			                <li>
			                  <a href="<?php echo site_url('Auth/change_password'); ?>" class="slide-item" hidden>Modifier son mot de passe</a>
			                </li>

			              </ul>
			            </li>

						<li class="slide" hidden="">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-lock"></i><span class="side-menu__label">Account</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="login.html" class="slide-item">Login</a>
								</li>
								<li>
									<a href="register.html" class="slide-item">Register</a>
								</li>
								<li>
									<a href="forgot-password.html" class="slide-item">Forgot password</a>
								</li>
								<li>
									<a href="lockscreen.html" class="slide-item">Lock screen</a>
								</li>

							</ul>
						</li>

					</ul>
				</aside>
	<?php
}
else{
	?>
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
				<aside class="app-sidebar">
					<div class="app-sidebar__user">
						<div class="dropdown user-pro-body">
							<div>
								<img src="<?php echo base_url().'assets'; ?>/images/faces/nsia.png" alt="user-img" class="avatar avatar-md brround">
							</div>
							<div class="user-info">
								<div class="mb-2">
								<a href="#" class="" data-toggle="" aria-haspopup="true" aria-expanded="false"> <span class="font-weight-semibold text-white"><?php echo $user->email//echo $user->first_name . '' . $user->last_name ; ?></span>  </a>
								<br><span class="text-gray"><?php echo $user->company ?></span>
								</div>
								<a hidden href="#" title="settings" class="user-button"><i class="fa fa-cogs"></i></a>
								<a href="<?php echo site_url().'/auth/profile'; ?>" title="profile" class="user-button"><i class="fa fa-user"></i></a>
								<a href="<?php echo site_url().'/auth/logout'; ?>" title="se déconnecter" class="user-button"><i class="fa fa-sign-out"></i></a>
							</div>
						</div>
					</div>
					
					<ul class="side-menu">
						
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-file-text-o"></i><span class="side-menu__label">MDI</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('Mdidata/add_new'); ?>" class="slide-item">Charger xlsx ,xls ou csv</a>
								</li>
								<li>
									<a href="<?php echo site_url('Mdidata/export_data'); ?>" class="slide-item">Exporter xlsx ,xls ou csv</a>
								</li>
							</ul>
						</li>

					</ul>
				</aside>
	<?php
}
?>


				

				<div class="app-content  my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title"><?php echo lang('index_heading');?></h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><?php echo lang('index_heading');?></a></li>
								<li class="breadcrumb-item active" aria-current="page">liste</li>
							</ol>

						</div>
						<div class="row">
							<div class="col-md-12 col-lg-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title"><?php echo lang('index_subheading');?></div>
								</div>
								<div class="card-body">
                                	<div class="table-responsive">

										<div id="infoMessage">
											<?php echo $message;?>
										</div>

										<p><?php echo anchor('auth/create_user', lang('index_create_user_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?></p>
										
										<table id="example" class="table table-striped table-bordered" style="width:100%" cellpadding=0 cellspacing=10>
											<thead>
												<tr>
													<th><?php echo lang('index_fname_th');?></th>
													<th><?php echo lang('index_lname_th');?></th>
													<th><?php echo lang('index_email_th');?></th>
													<th><?php echo lang('index_groups_th');?></th>
													<th><?php echo lang('index_status_th');?></th>
													<th><?php echo lang('index_action_th');?></th>
												</tr>
											</thead>
										    <tbody>
												<?php foreach ($users as $user):?>
												<tr>
													<td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
													<td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
													<td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
													<td>
														<?php foreach ($user->groups as $group):?>
															<?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
														<?php endforeach?>
													</td>
													<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
													<td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
												</tr>
												<?php endforeach;?>
											</tbody>
											
										</table>
										<?php echo $pagination; ?>										
										
									</div>
                                </div>
								<!-- table-wrapper -->
							</div>
							<!-- section-wrapper -->

							</div>
						</div>
					</div>
				</div>
			</div>

			<!--footer-->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
							Copyright © 2018 <a href="#">Plight</a>. Designed by <a href="#">Spruko</a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->
		</div>

		<!--Back to top-->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>

		<!-- Dashboard js -->
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/bootstrap.bundle.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/selectize.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/circle-progress.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/rating/jquery.rating-stars.js"></script>

		<!-- Fullside-menu Js-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/toggle-sidebar/sidemenu.js"></script>


		<!-- Data tables -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/datatable/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/datatable/dataTables.bootstrap4.min.js"></script>

		<!-- Select2 js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/select2/select2.full.min.js"></script>

		<!-- Custom scroll bar Js-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>
		<!-- Custom Js-->
		<script src="<?php echo base_url().'assets'; ?>/js/custom.js"></script>


		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
		


		<!-- Data table js -->
		<script>
			$(function(e) {
				$('#example').DataTable({
					dom: 'Bfrtip',
		        buttons: [
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdfHtml5'
		        ]
				});
			} );
		</script>

		<script type="text/javascript" language="javascript" >
		 $(document).ready(function() {
		    
		} );
		 
		</script>


	</body>

</html>