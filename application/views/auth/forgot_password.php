<link href="<?php echo base_url('assets/css1/bootstrap.min.css');?>" rel="stylesheet" id="bootstrap-css">
<script src="<?php echo base_url('assets/js1/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/jquery1/jquery.min.js');?>"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
	<title>Restaurer le mot de passe</title>
   <!--Made with love by Mutiullah Samim -->
   
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="<?php echo base_url('assets/css1/bootstrap413.min.css');?>" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="<?php echo base_url('assets/css1/all.css');?>" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css1/style.css'); ?>">
	
	<link rel="stylesheet" href="<?php echo base_url('assets/fonts/fonts/font-awesome.min.css'); ?>">
</head>
<body>
<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3><?php echo lang('forgot_password_heading');?></h3>
				<p style="color:white"><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
	
			</div>
			<div class="card-body">
			
			<div id="infoMessage" style="color:white"><?php echo $message;?></div>
				<?php echo form_open("auth/forgot_password");?>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-user"></i></span>
						</div>
							<?php //echo lang('login_identity_label', 'identity');?>
							<?php 
							$identity = array(
							'name'        => 'identity',
								'type'        => 'text',
							  'class'       => 'form-control',
							  'placeholder' => 'Email'
							);echo form_input($identity);?>
						
					</div>
					
					<div class="form-group">
						<?php 
						$sub = array(
							'type'        => 'submit',
							'value'        => 'Envoyer',
						  'class'       => 'btn float-left login_btn'
						);echo form_submit($sub);?>
					</div>
				<?php echo form_close();?>

      <!--<p>
      	<label for="identity"><?php //echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
      	<?php //echo form_input($identity);?>
      </p>
      <p><?php //echo form_submit('submit','Envoyer', lang('forgot_password_submit_btn'));?></p>
		-->
<?php //echo form_close();?>

					<div class="form-group">
						<a href="<?php echo base_url('auth/'); ?>" class="btn float-right login_btn">Annuler</a>
					</div>
	
			

			</div>

			
		</div>
	</div>
</div>
</body>
</html>
