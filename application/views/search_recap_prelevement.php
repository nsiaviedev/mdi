<?php


ini_set('max_execution_time',0);

// $conn = mysqli_connect("localhost", "root", "", "codeigniter");

// try
// {
// on se connecte enreFILE_1
// On se connecte à MySQL
// $pdo = new PDO('mysql:host=localhost;dbname=codeigniter;charset=utf8','root','');
// }
// catch(Exception $e)
// {
// En cas d’erreur, on affiche un message et on arrête tout
// die('Erreur : '.$e->getMessage());
// }

// $sql1 = "select table_name from information_schema.tables where TABLE_NAME LIKE '%copie%' ";

$user->first_name . '' . $user->last_name ;


?>
<!doctype html>
<html lang="en" dir="ltr">

<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="msapplication-TileColor" content="#0670f0">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" href="<?php echo base_url().'assets'; ?>/images/fav.ico" type="image/x-icon"/>

		<!-- Title -->
		<title>NSIA VIE ASSURANCE | ADMINISTRATION</title>
		<link rel="stylesheet" href="<?php echo base_url().'assets'; ?>/fonts/fonts/font-awesome.min.css">

		<!-- Font Family -->
		<link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

		<!-- Sidemenu Css -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/toggle-sidebar/sidemenu.css" rel="stylesheet" />

		<!-- Dashboard Css -->
		<link href="<?php echo base_url().'assets'; ?>/css/dashboard.css" rel="stylesheet" />

		<!-- c3.js Charts Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

		<!-- Data table css -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />

		<!-- Slect2 css -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/select2/select2.min.css" rel="stylesheet" />
		
		<!-- Time picker Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/time-picker/jquery.timepicker.css" rel="stylesheet" />

		<!-- Date Picker Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/date-picker/spectrum.css" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="<?php echo base_url().'assets'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />
		
		<!-- file Uploads -->
        <link href="<?php echo base_url().'assets'; ?>/plugins/fileuploads/css/dropify.css" rel="stylesheet" type="text/css" />
		
		<!---Font icons-->
		<link href="<?php echo base_url().'assets'; ?>/plugins/iconfonts/plugin.css" rel="stylesheet" />
</head>
	<body class="app sidebar-mini rtl">
		<div id="global-loader" ></div>
		<div class="page">
			<div class="page-main">
				
				<?php include('template/header.php'); ?>
	            <?php include('template/nav.php'); ?>

				<div class="app-content  my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">CRITERE DE RECHERCHE</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">recherche</a></li>
								<li class="breadcrumb-item active" aria-current="page">#</li>
							</ol>

						</div>
						
						
						
						<!-- recherce -->
						<div class="row">
							<!-- recherche par date et autres-->
							<div class="col-md-12 col-lg-12">
								
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><i class="fa fa-user"></i> PAR PERIODE(Mois envoi)</h3>
										
									</div>
									<div class="card-body">
                                        
										<form class="form-horizontal" action="<?php echo site_url(); ?>/PrelevementController/search" method="POST">
											<div class="row">
												<div class="col-md-12 col-lg-5">
													
													<div class="form-group">
														<label class="form-label">PERIODE</label>
														<input class="form-control" name="datedebut" type="text">
													</div>
												
												</div>
												
												<!--<div class="col-md-12 col-lg-5">-->
													<?php //var_dump($_POST['datedebut']); ?>
												<!--<div class="form-group">
														<label class="form-label">FIN</label>
														<input class="form-control" name="datefin" type="text">
													</div>
												
												</div>-->

												<div class="col-md-12 col-lg-2">
													<div class="form-group">
														<label class="form-label">&nbsp;</label>
														<input type="submit" name="ch_periode" value="Chercher" class="btn btn-primary"/>
													</div>
												</div>
												
												
												
												<div class="col-lg-6"hidden>
													<div class="card">
														<div class="card-header">
															<div class="card-title">DATE FIN</div>
														</div>
														<div class="card-body">
															<div class="wd-200 mg-b-30">
																<div class="input-group">
																	<div class="input-group-prepend">
																		<div class="input-group-text">
																			<i class="fa fa-calendar tx-16 lh-0 op-6"></i>
																		</div>
																	</div><input class="form-control fc-datepicker" placeholder="MM/DD/YYYY" type="text">
																</div>
															</div>
														</div>
													</div>
												</div>
							
											</div>

											<div class="row">
												

												<div class="col-md-12 col-lg-12">
													<div class="form-group">
														<label style="color: red" class="form-label">&nbsp;&nbsp; * Date au format AAAAMMJJ | exemple: 20240301</label>
													</div>
												</div>
												
											</div>

										</form>
									</div>
									
								</div>
								
							</div>
							
						</div>

					<!-- periode -->
					
					
					<?php
							if(isset($_POST['ch_periode']))
							{
								//motif == reclamation
								if($_POST['datedebut']!= "" /*and $_POST['datefin'] != ""*/)
								{
									$dated = $_POST['datedebut'];
									//$datef = $_POST['datefin'];
								
								
								
								?>
						<div class="row">
							<div class="col-md-12 col-lg-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">LISTE</div>
									<div id="recherche" >
										<div class="container">
											<div class="row">
												<div class="col-md-1" style="">
													<form method="post" action="<?php echo site_url(); ?>/PrelevementController/export_csv/<?php echo $dated ?>">
														<input type="submit" name="export" class="btn btn-info" value="Export xls" />
													</form>
												</div>

												<div hidden class="col-md-1" style="">
													<form method="post" action="<?php echo site_url(); ?>/PrelevementController/export_csv/<?php echo $dated ?>">
														<input type="submit" name="export" class="btn btn-success" value="Export xlsx" />
													</form>
												</div>

												<div hidden class="col-md-1" style="">
													<form method="post" action="<?php echo site_url(); ?>/PrelevementController/export_csv/<?php echo $dated ?>">
														<input type="submit" name="export" class="btn btn-warning" value="Export csv" />
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
								
                                	<div class="table-responsive">

                                		<?php 
                                		if(!empty($tuples)) {
										    $columns_names = array_keys($tuples[0]);
										 
										    echo '<table id="example" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>';
										    foreach($columns_names as $col) {
										        echo '<th class="wd-15p">'. $col .'</th>';
										    }
										    echo '</tr>
										    </thead>
										    <tbody>';
										    foreach($tuples as $tuple) {
										        echo '<tr>';
										        foreach($tuple as $col) {
										            echo '<td>'. $col .'</td>';
										        }
										        echo '</tr>';
										    }
										    }
										else {
										     echo '<tr colspan="5">
												<td>Aucune donnée...</td>
											</tr>';
										}
										    echo '</tbody>
										    </table>';
										
										?>
									
									</div>
                                </div>
								<!-- table-wrapper -->
							</div>
							<!-- section-wrapper -->

							</div>
						</div>
							<?php
							}elseif($_POST['datedebut'] == "" and $_POST['datefin']!= ""){
								echo '<script type="text/javascript"> alert("veuillez selectionner une date debut");</script>';
							}
							elseif($_POST['datedebut'] != "" /*and $_POST['datefin'] == ""*/){
								echo '<script type="text/javascript"> alert("veuillez selectionner une date de fin");</script>';
							}
							else
							{
								echo '<script type="text/javascript"> alert("veuillez selectionner une date debut et fin");</script>';
							}
						}
					?>
					
										
					</div>
				</div>
			</div>

			<!--footer-->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
							Copyright © 2020 <a href="#">NSIA VIE ASSURANCES</a>. Designed by <a href="#">DSI</a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->
		</div>

		<!--Back to top-->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>

		<!-- Dashboard js -->
		
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/bootstrap.bundle.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/jquery.sparkline.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/selectize.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/jquery.tablesorter.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/circle-progress.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/rating/jquery.rating-stars.js"></script>

		<!-- Fullside-menu Js-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/toggle-sidebar/sidemenu.js"></script>


		<!-- Data tables -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/datatable/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/datatable/dataTables.bootstrap4.min.js"></script>

		<!-- Select2 js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/select2/select2.full.min.js"></script>
		
		<!-- Inline js -->
		<script src="<?php echo base_url().'assets'; ?>/js/select2.js"></script>
		<!-- file uploads js -->
        <script src="<?php echo base_url().'assets'; ?>/plugins/fileuploads/js/dropify.js"></script>
		
		<!-- Custom scroll bar Js-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>
		
		<!-- Custom Js-->
		<script src="<?php echo base_url().'assets'; ?>/js/custom.js"></script>
		
		<!-- Timepicker js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/time-picker/jquery.timepicker.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/time-picker/toggles.min.js"></script>

		<!-- Datepicker js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/date-picker/spectrum.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/date-picker/jquery-ui.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/input-mask/jquery.maskedinput.js"></script>

		<!-- Data table js -->
		<script>
			
			$(function(e) {
				$('#example').DataTable({
					dom: 'Bfrtip',
		        buttons: [
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdfHtml5'
		        ]
				});
			} );
		</script>


	</body>


</html>