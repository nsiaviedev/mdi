<?php

ini_set('max_execution_time',0);

// $conn = mysqli_connect("localhost", "root", "", "codeigniter");

// try
// {
// on se connecte enreFILE_1
// On se connecte à MySQL
// $pdo = new PDO('mysql:host=localhost;dbname=codeigniter;charset=utf8','root','');
// }
// catch(Exception $e)
// {
// En cas d’erreur, on affiche un message et on arrête tout
// die('Erreur : '.$e->getMessage());
// }

// $sql1 = "select table_name from information_schema.tables where TABLE_NAME LIKE '%copie%' ";

$user->first_name . '' . $user->last_name ;


?>

<!doctype html>
<html lang="en" dir="ltr">
	
<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="msapplication-TileColor" content="#0670f0">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/fav.ico" />

		<!-- Title -->
		<title>NSIA - MISE A DISPOSITION D'INFORMATION</title>
		<link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>/fonts/fonts/font-awesome.min.css">

		<!-- Font Family -->
		<link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

		<!-- Sidemenu Css -->
		<link href="<?php echo base_url().'assets/'; ?>/plugins/toggle-sidebar/sidemenu.css" rel="stylesheet" />


		<!-- Dashboard Css -->
		<link href="<?php echo base_url().'assets/'; ?>/css/dashboard.css" rel="stylesheet" />

		<!-- c3.js Charts Plugin -->
		<link href="<?php echo base_url().'assets/'; ?>/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

		<!-- Data table css -->
		<link href="<?php echo base_url().'assets/'; ?>/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />

		<!-- Slect2 css -->
		<link href="<?php echo base_url().'assets/'; ?>/plugins/select2/select2.min.css" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="<?php echo base_url().'assets/'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

		<!---Font icons-->
		<link href="<?php echo base_url().'assets/'; ?>/plugins/iconfonts/plugin.css" rel="stylesheet" />
	</head>
	<body class="app sidebar-mini rtl">
		<div id="global-loader" ></div>
		<div class="page">
			<div class="page-main">
				<?php include('template/header.php'); ?>

				<!-- Sidebar menu-->
				<?php include('template/nav.php'); ?>

				

				<div class="app-content  my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Data Tables</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?php echo site_url(); ?>/Tableau/liste/<?php echo $tt ?>">Tables</a></li>
								<li class="breadcrumb-item active" aria-current="page">Data Tables</li>
							</ol>

						</div>
						<div class="row">
							<div class="col-md-12 col-lg-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Liste des données</div>
								</div>

								<div class="card-body">
									<div id="recherche" >
										<div class="container">
											<div class="row">
												<div class="col-md-1" style="">
													<form method="post" action="<?php echo site_url(); ?>/Tableau/export_csv/<?php echo $tt ?>">
														<input type="submit" name="export" class="btn btn-info" value="Export xls" />
													</form>
												</div>

												<div class="col-md-1" style="">
													<form method="post" action="<?php echo site_url(); ?>/Tableau/export_csv/<?php echo $tt ?>">
														<input type="submit" name="export" class="btn btn-success" value="Export xlsx" />
													</form>
												</div>

												<div class="col-md-1" style="">
													<form method="post" action="<?php echo site_url(); ?>/Tableau/export_csv/<?php echo $tt ?>">
														<input type="submit" name="export" class="btn btn-warning" value="Export csv" />
													</form>
												</div>
											</div>
										</div>
									</div>
                                	<div class="table-responsive">
                                		
                                		<?php 
                                		if(!empty($tuples)) {
										    $columns_names = array_keys($tuples[0]);
										 
										    echo '<table id="" class="table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>';
										    foreach($columns_names as $col) {
										        echo '<th class="wd-15p">'. $col .'</th>';
										    }
										    echo '</tr>
										    </thead>
										    <tbody>';
										    foreach($tuples as $tuple) {
										        echo '<tr>';
										        foreach($tuple as $col) {
										            echo '<td>'. $col .'</td>';
										        }
										        echo '</tr>';
										    }
										    }
										else {
										     echo '<tr colspan="5">
												<td>Aucune donnée...</td>
											</tr>';
										}
										    echo '</tbody>
										    </table>';
										
										?>
										
										<?php echo $this->pagination->create_links(); ?>
									</div>
                                </div>
								<!-- table-wrapper -->
							</div>
							<!-- section-wrapper -->

							</div>
						</div>
					</div>
				</div>
			</div>

			<!--footer-->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
							Copyright © 2018 <a href="#">Plight</a>. Designed by <a href="#">Spruko</a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->
		</div>

		<!--Back to top-->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>

		<!-- Dashboard js -->
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/bootstrap.bundle.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/selectize.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/circle-progress.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/rating/jquery.rating-stars.js"></script>

		<!-- Fullside-menu Js-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/toggle-sidebar/sidemenu.js"></script>


		<!-- Data tables -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/datatable/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/datatable/dataTables.bootstrap4.min.js"></script>

		<!-- Select2 js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/select2/select2.full.min.js"></script>

		<!-- Custom scroll bar Js-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>
		<!-- Custom Js-->
		<script src="<?php echo base_url().'assets'; ?>/js/custom.js"></script>


		<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script> -->
		


		<!-- Data table js -->
		<script>
			$(function(e) {
				$('#example').DataTable({
					dom: 'Bfrtip',
		        buttons: [
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdfHtml5'
		        ]
				});
			} );
		</script>

		<script type="text/javascript" language="javascript" >
		 $(document).ready(function() {
		    
		} );
		 
		</script>

		


	</body>

</html>