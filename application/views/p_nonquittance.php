<!doctype html>
<html lang="fr" dir="ltr">
	
<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="msapplication-TileColor" content="#0061da">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />


		<!-- Title -->
		<title>NSIA - MISE A DISPOSITION D'INFORMATION</title>
		<link rel="stylesheet" href="<?php echo base_url().'assets'; ?>/fonts/fonts/font-awesome.min.css">

		<!-- Font Family -->
		<link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

		<!-- Sidemenu Css -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/toggle-sidebar/sidemenu.css" rel="stylesheet" />


		<!-- Dashboard Css -->
		<link href="<?php echo base_url().'assets'; ?>/css/dashboard.css" rel="stylesheet" />

		<!-- c3.js Charts Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

		<!-- select2 Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/select2/select2.min.css" rel="stylesheet" />

		<!-- Time picker Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/time-picker/jquery.timepicker.css" rel="stylesheet" />

		<!-- Date Picker Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/date-picker/spectrum.css" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="<?php echo base_url().'assets'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

		<!-- file Uploads -->
        <link href="<?php echo base_url().'assets'; ?>/plugins/fileuploads/css/dropify.css" rel="stylesheet" type="text/css" />
		<!---Font icons-->
		<link href="<?php echo base_url().'assets'; ?>/plugins/iconfonts/plugin.css" rel="stylesheet" />
		<script type="text/javascript">
		$(document).ready(function() {
			$("#frmCSVImport").on("submit", function () {

		 //   $("#response").attr("class", "");
			// 	$("#response").html("");
			// 	var fileType = ".";
			// 	var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
			// 	if (!regex.test($("#file").val().toLowerCase())) {
			// 	   $("#response").addClass("error");
			// 	   $("#response").addClass("display-block");
			// 		$("#response").html("Invalid File. Upload : <b>" + fileType + "</b> Files.");
			// 		return false;
			// 	}
			// 	return true;
			// });
		});
		</script>

		<script >

            function display_loader()
			{
			     document.getElementById('loader').innerHTML = "<div class='lox'><img class='' src='<?php echo base_url('assets/images/loader.svg'); ?>' alt='load'/>Veuillez patienté pendant le chargement du fichier...</div>";
			}
        </script>

		<style>
			.success {
				background: #c7efd9;
				border: #bbe2cd 1px solid;
			}

			.error {
				background: #fbcfcf;
				border: #f3c6c7 1px solid;
			}
			.test {
				//border: 1px solid red;
				position: relative;
				top: -8px
			}
			.lo{
				position: fixed;
				z-index: 9000;
				left: 0;
				top: 0;
				right: 0;
				bottom: 0;
				margin: 0 auto;
			}

			.lox{
				margin-left:500px;
			}
		</style>
	</head>

	<?php 
	    		if(!empty($oo)) {

	    			echo '<script>
					$(function testverifs(){
						console.log("tampe is ready to disable");
						//Begin
						$("#pppp").hide();
						$("#ppp2").hide();
						$(".okok").hide();
						$(".page-mains").hide();
							
					});
				</script>';

			
				    }
				
				?>
<body class="app sidebar-mini rtl">
<div id="/global-loader/" ></div>
<div class="page">
<div class="page-mains">
<?php include('template/header.php'); ?>

<!-- Sidebar menu-->
<?php include('template/nav.php'); ?>

<div class="app-content  my-3 my-md-5">
<div class="side-app">
<div class="page-header">
<h4 class="page-title">MDI</h4>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">MDI</a></li>
<li class="breadcrumb-item active" aria-current="page">Charger un fichier</li>
</ol>

</div>


	    		
	    		

<div class="row">
<div class="col-md-12">
<form class="card" action="<?php base_url('PoliceController/show_quittance');  ?>" method="post" name="frmCSVImport" id="frmCSVImport" enctype="multipart/form-data">
<?php //echo form_open_multipart('mdidata/add_new');?>
	<div class="card-header okok">
		<h3 class="card-title">Charger un fichier  <?php  ?></h3>
	</div>

	<span id="success_message"></span>

	<div class="card-body">
		<div class="row">
			<div class="col-md-12 okok">
				<input type="file" name="file" id="file" accept="." class="dropify" data-height="110" />
				<span id="upload_error"></span>
			</div>
			

			<div class="col-md-6">
                     <?php 
						if(!empty($result_msg)) { echo '<span class="alert alert-success">'.$result_msg.'</span>'; }
					?>
				<div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>">
					<?php 
						if(!empty($message)) { echo $message; }
					?>
					<?php if($this->session->has_userdata('error_file')){ ?>
						<div class="alert alert-warning" role="alert">
						<?php echo $this->session->flashdata('error_file'); ?>
						</div>
					<?php } ?>
					<?php if($this->session->has_userdata('success')){ ?>
						<div class="alert alert-primary" role="alert">
						<?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php } ?>
					<?php if($this->session->has_userdata('error')){ ?>
						<div class="alert alert-danger" role="alert">
						<?php echo $this->session->flashdata('error'); ?>
						</div>
					<?php } ?>
				</div>
			</div>


		</div>
		
	</div>

	<div id="loader">
	</div>

	
	<div class="card-body">
		<div class="form-footer">
			<button type="submit" onclick="display_loader()" id="submit" name="import" class="btn btn-primary btn-block">Extraire</button>
		</div>
	</div>
</form>

<div class="form-group" id="process" style="display: none;">
		<div class="progress">
			<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valumax="100"></div>
		</div>
	</div>

						
</div>

</div>


</div>
</div>
</div>

			

			<!--footer-->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
							Copyright © <?php echo date('Y'); ?> <a href="#">NSIA VIE & ASSURANCE</a>. All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->
		</div>

		<!-- Back to top -->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>
		<!-- Dashboard Css -->
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/bootstrap.bundle.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/jquery.sparkline.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/selectize.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/jquery.tablesorter.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/js/vendors/circle-progress.min.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/rating/jquery.rating-stars.js"></script>

		<!-- Fullside-menu Js-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/toggle-sidebar/sidemenu.js"></script>


		<!--Select2 js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/select2/select2.full.min.js"></script>

		<!-- Timepicker js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/time-picker/jquery.timepicker.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/time-picker/toggles.min.js"></script>

		<!-- Datepicker js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/date-picker/spectrum.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/date-picker/jquery-ui.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/input-mask/jquery.maskedinput.js"></script>

		<!-- Inline js -->
		<script src="<?php echo base_url().'assets'; ?>/js/select2.js"></script>
		<!-- file uploads js -->
        <script src="<?php echo base_url().'assets'; ?>/plugins/fileuploads/js/dropify.js"></script>

		<!-- Custom scroll bar Js-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- Custom Js-->
		<script src="<?php echo base_url().'assets'; ?>/js/custom.js"></script>

		<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script> -->

		<script >
            $('.dropify').dropify({
                messages: {
                    'default': 'Glisser pour deposer votre fichier ou cliquez pour charger',
                    'replace': 'Glisser pour deposer votre fichier ou cliquez pour remplacer',
                    'remove': 'Supprimer',
                    'error': 'Ooops, something wrong appended.'
                },
                error: {
                    'fileSize': 'The file size is too big (2M max).'
                }
            });
        </script>

        <script>
			$(function(e) {
				$('#example').DataTable({
					dom: 'Bfrtip',
		        buttons: [
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdfHtml5'
		        ]
				});
			} );
		</script>

		<script>
		$(function testverif(){
			console.log('is ready to disable');
			//Begin
			$(".hidchp").hide();
			$(".hidchq").hide();
			$(".hidchi").hide();
			$(".hidcha").hide();
			$(".hidchco").hide();
			$(".hidchpay").hide();
			$(".hidchcompte").hide();
			$(".hidchassu").hide();
			$(".hidchchapchap").hide();
			$(".hidchencaiss").hide();
			
			//Second option to display
			$('#shchp').on('change',function(){
				if(document.getElementById("shchp").checked == true){
					console.log('is ready to enable shchp');
					$(".hidchp").show();
				}else{
					console.log('is ready to disable shchp');
					$(".hidchp").hide();
					document.getElementById("shchp1").checked = false;
					document.getElementById("shchp2").checked = false;
					document.getElementById("shchp3").checked = false;
					document.getElementById("shchp4").checked = false;
					document.getElementById("shchp5").checked = false;
					document.getElementById("shchp6").checked = false;
					document.getElementById("shchp7").checked = false;
					document.getElementById("shchp8").checked = false;
					document.getElementById("shchp9").checked = false;
					document.getElementById("shchp10").checked = false;
					document.getElementById("shchp11").checked = false;
				}
			});

			//Second option to display
			$('#shchq').on('change',function(){
				if(document.getElementById("shchq").checked == true){
					console.log('is ready to enable shchq');
					$(".hidchq").show();
				}else{
					console.log('is ready to disable shchq');
					$(".hidchq").hide();
					document.getElementById("shchq1").checked = false;
					document.getElementById("shchq2").checked = false;
					document.getElementById("shchq3").checked = false;
					document.getElementById("shchq4").checked = false;
					document.getElementById("shchq5").checked = false;
					document.getElementById("shchq6").checked = false;
					document.getElementById("shchq7").checked = false;
					document.getElementById("shchq8").checked = false;
					document.getElementById("shchq9").checked = false;
					document.getElementById("shchq10").checked = false;
					document.getElementById("shchq11").checked = false;
					document.getElementById("shchq12").checked = false;
					document.getElementById("shchq13").checked = false;
					document.getElementById("shchq14").checked = false;
					document.getElementById("shchq15").checked = false;
					document.getElementById("shchq16").checked = false;
					document.getElementById("shchq17").checked = false;
					document.getElementById("shchq18").checked = false;
					document.getElementById("shchq19").checked = false;
				}
			});

			//Second option to display
			$('#shchi').on('change',function(){
				if(document.getElementById("shchi").checked == true){
					console.log('is ready to enable shchi');
					$(".hidchi").show();
				}else{
					console.log('is ready to disable shchi');
					$(".hidchi").hide();
					document.getElementById("shchi1").checked = false;
					document.getElementById("shchi2").checked = false;
					document.getElementById("shchi3").checked = false;
					document.getElementById("shchi4").checked = false;
					document.getElementById("shchi5").checked = false;
				}
			});

			//Second option to display
			$('#shcha').on('change',function(){
				if(document.getElementById("shcha").checked == true){
					console.log('is ready to enable shcha');
					$(".hidcha").show();
				}else{
					console.log('is ready to disable shcha');
					$(".hidcha").hide();
					document.getElementById("shcha1").checked = false;
					document.getElementById("shcha2").checked = false;
					document.getElementById("shcha3").checked = false;
					document.getElementById("shcha4").checked = false;
					document.getElementById("shcha5").checked = false;
					document.getElementById("shcha6").checked = false;
					document.getElementById("shcha7").checked = false;
					document.getElementById("shcha8").checked = false;
				}
			});

			//Second option to display
			$('#shchpay').on('change',function(){
				if(document.getElementById("shchpay").checked == true){
					console.log('is ready to enable shchpay');
					$(".hidchpay").show();
				}else{
					console.log('is ready to disable shchpay');
					$(".hidchpay").hide();
					document.getElementById("shchpay1").checked = false;
					document.getElementById("shchpay2").checked = false;
					document.getElementById("shchpay3").checked = false;
					document.getElementById("shchpay4").checked = false;
					document.getElementById("shchpay5").checked = false;
				}
			});

			//Second option to display
			$('#shchco').on('change',function(){
				if(document.getElementById("shchco").checked == true){
					console.log('is ready to enable shchco');
					$(".hidchco").show();
				}else{
					console.log('is ready to disable shchco');
					$(".hidchco").hide();
					//document.getElementById("shchco").checked = false;
				}
			});

			//Second option to display
			$('#shchcompte').on('change',function(){
				if(document.getElementById("shchcompte").checked == true){
					console.log('is ready to enable shchcompte');
					$(".hidchcompte").show();
					//$(".shchp10").show();
					document.getElementById("shchp10").checked = true;
				}else{
					console.log('is ready to disable shchcompte');
					$(".hidchcompte").hide();
					document.getElementById("shchcompte1").checked = false;
					// document.getElementById("shchcompte2").checked = false;
					// document.getElementById("shchcompte3").checked = false;
					// document.getElementById("shchcompte4").checked = false;
					document.getElementById("shchp10").checked = false;
				}
			});

			//Second option to display
			$('#shchassu').on('change',function(){
				if(document.getElementById("shchassu").checked == true){
					console.log('is ready to enable shchassu');
					$(".hidchassu").show();
				}else{
					console.log('is ready to disable shchassu');
					$(".hidchassu").hide();
					document.getElementById("shchassu1").checked = false;
					document.getElementById("shchassu2").checked = false;
					document.getElementById("shchassu3").checked = false;
					document.getElementById("shchassu4").checked = false;
					document.getElementById("shchassu5").checked = false;
				}
			});

			//affiche les données shchchapchap
			$('#shchchapchap').on('change',function(){
				if(document.getElementById("shchchapchap").checked == true){
					console.log('is ready to enable shchchapchap');
					$(".hidchchapchap").show();
				}else{
					console.log('is ready to disable shchchapchap');
					$(".hidchchapchap").hide();
					document.getElementById("shchchapchap1").checked = false;
					document.getElementById("shchchapchap2").checked = false;
				}
			});

			//affiche les données shchencaiss
			$('#shchencaiss').on('change',function(){
				if(document.getElementById("shchencaiss").checked == true){
					console.log('is ready to enable shchencaiss');
					$(".hidchencaiss").show();
				}else{
					console.log('is ready to disable shchencaiss');
					$(".hidchencaiss").hide();
					document.getElementById("shchencaiss1").checked = false;
					document.getElementById("shchencaiss2").checked = false;
					document.getElementById("shchencaiss3").checked = false;
					document.getElementById("shchencaiss4").checked = false;
					document.getElementById("shchencaiss5").checked = false;
				}
			});
				
		});
	</script>
        

	</body>
</html>


