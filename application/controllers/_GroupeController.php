<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('display_errors', 'on');
// phpinfo();
ini_set('max_execution_time',0);

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
 //Load plugin
require (APPPATH .'third_party/vendor/autoload.php');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use PhpOffice\PhpSpreadsheet\Shared\Date;

require_once(APPPATH .'libraries/PHPExcel.php');


class GroupeController extends CI_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('excel');
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);
		$this->load->model('Mdi_model','mdi');
		$this->load->model('Export_model', 'export');
		$this->load->model('Table_model', 'umodel');
		$this->load->library('pagination');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
	}


	public function index() {
	
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			//show_error('You must be an administrator to view this page.');
			redirect('GroupeController/add_newg', 'refresh');
		}
		else
		{
			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();

			$user = $this->ion_auth->user()->row();
			$data['user'] = $user;
			
			//USAGE NOTE - you can do more complicated queries like this
			//$this->data['users'] = $this->ion_auth->where('field', 'value')->users()->result();
			
			// foreach ($this->data['users'] as $k => $user)
			// {
				// $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			// }

			$this->_render_page('' . DIRECTORY_SEPARATOR . 'index', $data);
		}
	}


	public function add_newg($datas = []){

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}else {
			
			$user = $this->ion_auth->user()->row();
		
			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();

			$data['user'] = $user;

			$tim = time();//recupère dans $tim le timestamp
			$txt = preg_replace('/\s/','',$user->first_name.''.$user->last_name);//supprime les espaces

			
			if (isset($_POST["import"])){
				//charge un fichier
				//echo "ficher chargé";
				$fileNames = $_FILES["file"]["tmp_name"];
				$fileName = $_FILES["file"]["name"];
				$chk_ext = explode(".",$fileName);
				$table = explode(".",$fileName);

				if (!file_exists($_FILES['file']['tmp_name']) || !is_uploaded_file($_FILES['file']['tmp_name'])) 
				{
				    
					$this->session->set_flashdata('error_file','Vous n\'avez pas chargé le fichier.'); 
				    redirect('GroupeController/add_newg','refresh');
				}
				else
				{
				    // Your file has been uploaded
				    if(strtolower($chk_ext[1]) == "csv" || strtolower($chk_ext[1]) == "xls" || strtolower($chk_ext[1]) == "xlsx" ){
						if($_FILES["file"]["size"] > 0){

							$ok = $tim.$txt.'_'.'tampe2.csv';
						
							$xlsx = PHPExcel_IOFactory::load($fileNames);
							$writer = PHPExcel_IOFactory::createWriter($xlsx, 'CSV');
							
							$writer->setDelimiter(";");
							$writer->setEnclosure("");
							$writer->save('assets/file_upload/'.$ok);
						
							$row = 1;
							$file = fopen('assets/file_upload/'.$ok, "r");
							$i = 0;

							$ligne1=fgetss($file,4096);
							$array=array();
							$ar=explode(";",$ligne1);

							/* verifier si une table existe si oui la supprimer*/
							$arrays = array(0=>$ar[0]);
							$ar[0]=(isset($ar[0]) ) ? $ar[0] : Null;
							$champs1=explode(";",$ar[0]);

							//création de la table
							
							$sqlCreateTable = "CREATE TABLE $table[0]$txt$tim (id int IDENTITY(1,1) PRIMARY KEY )";
							// $sqlCreateTable = "CREATE TABLE $table[0]$txt$tim (tempon VARCHAR(1000) )";
							// echo $sqlCreateTable;
							$stmtCreateTable = $this->db->query($sqlCreateTable);

							if(!empty($ar)){
								foreach ($ar as $key => $val) {
									$val1 = $ar[$key];
									$tampe = preg_replace('/\s/', '',$val1);
									// var_dump($val1);
									//insertion des colonnes du fichier excel dans la table créé
									$sqlInsertColumn = "ALTER TABLE $table[0]$txt$tim ADD $tampe VARCHAR(1000) ";
									// echo $sqlInsertColumn;
									$stmtInsertColumn = $this->db->query($sqlInsertColumn);
									$obaGetEntete[] = $tampe;
								}
							}
							
							
							//creation de function
							function isFloat($var){
								if(!is_scalar($var)){
									return false;
								}
								return is_float($var+0);
							}

							function formatMoney($number, $cents = 1) { // cents: 0=never, 1=if needed, 2=always
								if (is_numeric($number)) { // a number
								  if (!$number) { // zero
									$money = ($cents == 2 ? '0.00' : '0'); // output zero
								  } else { // value
									if (floor($number) == $number) { // whole number
									  $money = number_format($number); // format
									} else { // cents
									  $money = number_format(round($number), ($cents == 0 ? 0 : 2)); // format
									} // integer or decimal
								  } // value
								  return $money;
								} // numeric
							}

							function number_format_drop_zero_decimals($n, $n_decimals){
								return ((floor($n) == round($n, $n_decimals)) ? number_format($n) : number_format($n, $n_decimals));
							}
							
							$inputFileType = PhpOffice\PhpSpreadsheet\IOFactory::identify($fileNames);
			  
							$reader = PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
							$reader->setReadDataOnly(false);
							$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileNames);
							
							$worksheet = $spreadsheet->getActiveSheet();

							//begin foreach
							foreach ($worksheet->getRowIterator() as $pCoordinate) {
								$cellIterator = $pCoordinate->getCellIterator();
								$cellIterator->setIterateOnlyExistingCells(FALSE);
								$cells = [];
								foreach ($cellIterator as $cell) {
									// $pCell =$worksheet->getCell($pCoordinate);
									$isDateTime = Date::isDateTime($cell);
									
									if($isDateTime){
										$cells[] = \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($cell->getValue(), 'DD/MM/YYYY');
										// print_r($cells);
									}elseif(is_numeric($cell->getValue())){
										if(isFloat($cell->getValue())){
											//print_r($va.'<br/>');	
											// print_r(formatMoney($va, 0).'<br/>');				  
											$okk = number_format_drop_zero_decimals($cell->getValue(), 0);
											$nombre = str_replace(',', '', $okk);										  
											//$vv = formatMoney($va, 0); 
											// print_r($nombre->getValue().'<br/>');
											// $cells[] = $nombre;
											array_push($cells,$nombre);
										}
									}else{
										$cells[] = $cell->getValue();
									}	
								}
								//print_r($cells);
								$escaped_values = array_map(null,array_values($cells));
								// $escaped_values = array_map(null,array_values($cellss));
								$txtx = array();
								$txtx = str_replace(array("'"), " ",$escaped_values);

								$newdata = "'" . implode("','", $txtx) . "'";
								
								// var_dump($newdata);

								//si le fichier n'est pas vide
								if(!empty($newdata)){
									try{
										//création de la table
										$sqlInsertData = "INSERT INTO $table[0]$txt$tim  VALUES ($newdata)";
										// echo $sqlCreateTable;
										$stmtInsertColumn = $this->db->query($sqlInsertData);

									} catch(PDOException $e){
										echo $sqlInsertData. "<br/>" . $e->getMessage();
									}
								}
							}
							//end foreach

							// $return = $this->mdi->insert_json_in_db($tab,$json_data);
							if($stmtInsertColumn == 1){
								$data['result_msg'] = "Data successfuly inserted";
							}else{
								$data['result_msg'] = "Please configure your database correctly";
							}
							

							// Delete first row of table

							// $sqlDeleteFirstRow = "DELETE TOP(1) FROM $table[0]$txt$tim";
							$sqlDeleteFirstRow = "DELETE FROM $table[0]$txt$tim WHERE id = 1 ";
							// echo $sqlDeleteFirstRow;
							$stmtDeleteFirstRow = $this->db->query($sqlDeleteFirstRow);

							$principale = "$table[0]$txt$tim";

				 			$allObaDataGroup = $this->obaExtraitGroup($principale, $_POST['JOINTABLES'], $_POST['JOINCHAMPS'],$obaGetEntete);
				 			//$allObaDatas = $this->test($rest);


						}
					}
				}
				
			}
			
			$this->_render_page('' . DIRECTORY_SEPARATOR . 'add_groupe', $data);
			//$this->load->view('add_groupe',$data);
		}

	}


	public function export_data(){

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}else {

			$user = $this->ion_auth->user()->row();
		
			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();

			$data['user'] = $user;

			//affiche la liste des table
			//$data['tbl_name'] = $this->mdi->get_all_table_query($user->first_name . '' . $user->last_name);
			//die();


			// $config['base_url'] = base_url().'Mdidata/export_data';
			// $config['total_rows'] = 200;
			// $config['per_page'] = 20;

			// $this->pagination->initialize($config);

			// $data['pagination'] = $this->pagination->create_links();
			// $data['tuples'] = $this->mdi->get_table_from_user_query();
			
			// $config['base_url'] = base_url().'Mdidata/export_data';        
			// $config['total_rows'] = $this->mdi->get_count_sondage_query($table[0]);
			
			// $config['per_page'] = 50;        
			// $config['uri_segment'] = 3;        
			// $config['use_page_numbers'] = TRUE;        
			// $config['full_tag_open'] = '<ul class="pagination">';        
			// $config['full_tag_close'] = '</ul>';  
			// $config['attributes'] = array('class' => 'page-link');   
			// $config['first_link'] = 'First';        
			// $config['last_link'] = 'Last';   
			// $config['first_tag_open'] = '<li>';        
			// $config['first_tag_close'] = '</li>';        
			// $config['prev_link'] = '&laquo';        
			// $config['prev_tag_open'] = '<li class="prev">';        
			// $config['prev_tag_close'] = '</li>';        
			// $config['next_link'] = '&raquo';        
			// $config['next_tag_open'] = '<li>';        
			// $config['next_tag_close'] = '</li>';        
			// $config['last_tag_open'] = '<li>';        
			// $config['last_tag_close'] = '</li>';        
			// $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';        
			// $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';        
			// $config['num_tag_open'] = '<li>';
			// $config['num_tag_close'] = '</li>';
			// $config['num_links'] = 1;

			
			// $this->pagination->initialize($config);   
			// // $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			// $page = $this->uri->segment(3);
			// $start = ($page - 0) * $config["per_page"];
			// $data['pagination'] = $this->pagination->create_links();
			$ok = $user->first_name.''.$user->last_name;
			$data['tuples'] = $this->umodel->get_all_table_query($ok);


			$this->_render_page('' . DIRECTORY_SEPARATOR . 'alldata', $data);
			// $this->load->view('datatable',$data);
		}

	}


	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}


	public function obaExtraitGroup($tab_principale, $tab_secondaires, $tab_champs, $getEntetes)
	{//,$getStatGrpBy

		// $serverName1 = '10.10.3.70\Sunntech'; 
  //       $database1 = 'SUNDB_RCI';
  //       $uid1 = "reportvie";
  //  		$pwd1 = "reportvie";
 		
 		

  //  		$server_name = "10.10.3.70\Sunntech";
		//  $database_name = "SUNDB_RCI";
		//  try
		//   {
		//    $conn1 = new PDO("sqlsrv:Server=$server_name;Database=$database_name;ConnectionPooling=0", "reportvie", "reportvie");
		//    $conn1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// }
		// catch(PDOException $e)
		// {

		//     $e->getMessage();
		//     echo $e;

		// }
		
		$police = "NSIACIF.JPPOLIP";
		$quittance = "NSIACIF.JPQUITP";
		$agent = "NSIACIF.JAAGENP";
		$payeur = "NSIACIF.JAIDENP PAYEUR";
		$client = "NSIACIF.JAIDENP CLIENT";
		$compte_bancaire = "NSIACIF.JACORBP BANCAIRE";
		$assures = "NSIACIF.JAIDENP ASSURES";
		$conventions ="NSIACIF.JPPLANP CONVENTION";

		$id_police = "NUMERO_POLICE";
		$id_quittance = "NUMERO_QUITTANCE";
		$aggregation_1 ="MAX";
		$aggregation_2 ="COUNT";

		$id_agent=$id_identite="";
		$filtre=[];
		// $grouper=['(MAX)','(COUNT)','SDSD'];

		$query2 = $this->db->query("SELECT * FROM $tab_principale");
 		$result2 = $query2->result_array();
 		$oook = array_keys($result2[0]);
 		


 		
 		$the_array = array_merge($oook,$tab_champs);

		$tab_champ =  ( (count($the_array)>1) ? implode(',', $the_array) : $the_array[0] ) ;
		// $tab_champ_p =  ( (count($oook)>1) ? implode(',', $oook) : $oook[0] ) ;
		// $getBcivietable = "[LINK_BCIVIE].[B_CIVIE].[DBO]";
        
		$oba_extraction = "SELECT ".$tab_champ." FROM [LINK_BCIVIE].[B_CIVIE].[DBO].".$tab_principale." A ";

		
		foreach ($tab_secondaires as $keys => $tab_secondaire) {
			
			if (strpos($tab_secondaire, $police) !== false) {
				$filtre[] = " JPPOLIP_WNUPO>0";
				
		        $oba_extraction .= "".$tab_secondaire;
			}

			if (strpos($tab_secondaire, $quittance) !== false) {

				$filtre[] = "MPYCO>0";

				if (array_search($id_quittance, $getEntetes)!==false) {
					
					

					$tab_secondaire_reecrit_1 = str_replace("%numero%", "NUMERO_QUITTANCE", $tab_secondaire);
					$tab_secondaire_reecrit_2 = str_replace("%idbd%", "WNUCO", $tab_secondaire_reecrit_1);

					$oba_extraction .= " ".$tab_secondaire_reecrit_2;
				}else {
					// var_dump($getEntetes);
					// die('ok nooooooooooo');
					$tab_secondaire_reecrit_1 = str_replace("%numero%", "NUMERO_POLICE", $tab_secondaire);
					$tab_secondaire_reecrit_2 = str_replace("%idbd%", "WNUPO", $tab_secondaire_reecrit_1);

					$oba_extraction .= " ".$tab_secondaire_reecrit_2;
				}
			}
			if (strpos($tab_secondaire, $agent) !== false) {
								
		        $oba_extraction .= " ".$tab_secondaire;
			} 

			if (strpos($tab_secondaire, $payeur) !== false) {
								
		        $oba_extraction .= " ".$tab_secondaire;
			}
			if (strpos($tab_secondaire, $client) !== false) {
								
		        $oba_extraction .= " ".$tab_secondaire;
			}
			if (strpos($tab_secondaire, $compte_bancaire) !== false) {
								
		        $oba_extraction .= " ".$tab_secondaire;
			}

			if (strpos($tab_secondaire, $assures) !== false) {
								
		        $oba_extraction .= " ".$tab_secondaire;
			}

			if (strpos($tab_secondaire, $conventions) !== false) {
								
		        $oba_extraction .= " ".$tab_secondaire;
			}

		}


			
		if (!empty($filtre)) {

				$oba_extraction .= " WHERE ".( (count($filtre)>1) ? implode(' AND ', $filtre) : $filtre[0] );
		}

		

	     $deleteKeys=$setGroupby = array();
		 $agregats = array('COUNT','MAX');
		 $the_keys = '';

			// parcours des données
			foreach($the_array as $key => $champ)
			{
				 $the_keys = $key;
				 $setGroupby[] = $champ;
			  //parcours des mots-clés
			  foreach($agregats as $search)
			  {
			    // stockage des résultats positifs
			    if(strpos($champ,$search)!==false)
			    {
			        $deleteKeys[] = $the_keys;
			    }

			  }
			}

			foreach ($setGroupby as $key => $setGroup) {

				foreach($deleteKeys as $deleteKey){

				    // stockage des résultats positifs
				    if( $key == $deleteKey )
			    	{
			        	// $deleteKeys[] = $the_keys;
			        	unset($setGroupby[$key]);
			    	}
				}
			}

			
			$newSetGroupBy = array();


			foreach ($setGroupby as $key => $setGB) {

				if (strpos($setGB, 'CASE') !== false) {
					$newSetGroupBy[] = 'BANCAIRE.JACORBP_BANXD';
				}elseif (strpos($setGB, 'AS ') !== false) {
					$newSetGroupBy[] = substr($setGB,0,strpos($setGB, "AS "));
				}else{
					$newSetGroupBy[] = $setGB;
				}
				
			}

			$oba_extraction .= " GROUP BY ".( (count($newSetGroupBy)>1 ) ?  implode(' , ', $newSetGroupBy)  : $newSetGroupBy[0]);

				
			$obaExtraitTout = "SELECT DISTINCT * FROM OPENQUERY([LINK_SUNSHINE],'$oba_extraction')";

		 $requete =  $this->db->query($obaExtraitTout);

		 // $result = $requete->fetchAll(PDO::FETCH_ASSOC);


		 $user = $this->ion_auth->user()->row();
	
		$this->data['title'] = $this->lang->line('index_heading');
		
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		//list the users
		$this->data['users'] = $this->ion_auth->users()->result();

		$data['user'] = $user;

		$tim = time();//recupère dans $tim le timestamp
		$txt = preg_replace('/\s/','',$user->first_name.''.$user->last_name);//supprime les espaces


		$newtable = $txt.$tim;


		//$data['oo'] = $result;

		//$jso = json_encode($result);
		//echo "<br/>";

		// $sqlCreateTable = "CREATE TABLE $newtable (idnew VARCHAR(MAX) )";
		// $sqlCreateTable = "CREATE TABLE $table[0]$txt$tim (tempon VARCHAR(1000) )";
		//echo $sqlCreateTable;
		// $stmtCreateTable = $this->db->query($sqlCreateTable);

		//Insertion dans la nouvelle table cree
		// $sqlInsertData = "INSERT INTO $newtable(idnew) VALUES ('$jso')";
		//echo $sqlInsertData;
		// die();
		// $stmtInsertColumn = $this->db->query($sqlInsertData);
		
		//test de la nouvelle requete
		
		$req1 = "SELECT * INTO $newtable FROM ($obaExtraitTout)T ";
		
		$stmtInsertColumn = $this->db->query($req1);
	

		// $this->load->view('tabletest',$data);
 
		if(isset($stmtInsertColumn1)){
			redirect('Mdidata/export_data');
		}else{
			die("redirect is not possible");
		}

		
	}



}

?>