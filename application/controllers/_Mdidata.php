<?php defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('display_errors', 'on');
// phpinfo();
ini_set('max_execution_time',0);

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
 //Load plugin
require (APPPATH .'third_party\vendor\autoload.php');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use PhpOffice\PhpSpreadsheet\Shared\Date;


class Mdidata extends CI_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('excel');
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);
		$this->load->model('Mdi_model','mdi');
		$this->load->model('Export_model', 'export');
		$this->load->model('Table_model', 'umodel');
		$this->load->library('pagination');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
	}
	
	public function index() {
	
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			//show_error('You must be an administrator to view this page.');
			redirect('Mdidata/add_new', 'refresh');
		}
		else
		{
			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();

			$user = $this->ion_auth->user()->row();
			$data['user'] = $user;
			
			//USAGE NOTE - you can do more complicated queries like this
			//$this->data['users'] = $this->ion_auth->where('field', 'value')->users()->result();
			
			// foreach ($this->data['users'] as $k => $user)
			// {
				// $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			// }

			$this->_render_page('' . DIRECTORY_SEPARATOR . 'index', $data);
		}
	}
	
	public function add_new($datas = [])
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}else {
			
			$user = $this->ion_auth->user()->row();
		
			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();

			$data['user'] = $user;

			

			$tim = time();//recupère dans $tim le timestamp
			$txt = preg_replace('/\s/','',$user->first_name.''.$user->last_name);//supprime les espaces

			if (isset($_POST["import"])){
				//charge un fichier
				//echo "ficher chargé";
				$fileNames = $_FILES["file"]["tmp_name"];
				$fileName = $_FILES["file"]["name"];
				$chk_ext = explode(".",$fileName);
				$table = explode(".",$fileName);

				if(strtolower($chk_ext[1]) == "csv" || strtolower($chk_ext[1]) == "xls" || strtolower($chk_ext[1]) == "xlsx" ){
					if($_FILES["file"]["size"] > 0){

						$ok = $tim.$txt.'_'.'tampe2.csv';
					
						$xlsx = PHPExcel_IOFactory::load($fileNames);
						$writer = PHPExcel_IOFactory::createWriter($xlsx, 'CSV');
						
						$writer->setDelimiter(";");
						$writer->setEnclosure("");
						$writer->save('assets/file_upload/'.$ok);
					
						$row = 1;
						$file = fopen('assets/file_upload/'.$ok, "r");
						$i = 0;

						$ligne1=fgetss($file,4096);
						$array=array();
						$ar=explode(";",$ligne1);

						/* verifier si une table existe si oui la supprimer*/
						$arrays = array(0=>$ar[0]);
						$ar[0]=(isset($ar[0]) ) ? $ar[0] : Null;
						$champs1=explode(";",$ar[0]);

						//création de la table
						$sqlCreateTable = "CREATE TABLE $table[0]$txt$tim (id int IDENTITY(1,1) PRIMARY KEY )";
						// $sqlCreateTable = "CREATE TABLE $table[0]$txt$tim (tempon VARCHAR(1000) )";
						// echo $sqlCreateTable;
						$stmtCreateTable = $this->db->query($sqlCreateTable);

						if(!empty($ar)){
							foreach ($ar as $key => $val) {
								$val1 = $ar[$key];
								$tampe = preg_replace('/\s/', '',$val1);
								// var_dump($val1);
								//insertion des colonnes du fichier excel dans la table créé
								$sqlInsertColumn = "ALTER TABLE $table[0]$txt$tim ADD $tampe VARCHAR(1000) ";
								// echo $sqlInsertColumn;
								$stmtInsertColumn = $this->db->query($sqlInsertColumn);
							}
						}

						// suppression du champ tempon créé
						// $sqlDeleteColumn = "ALTER TABLE $table[0]$txt$tim DROP COLUMN tempon";
						// // echo $sqlDeleteColumn;
						// $stmtInsertColumn = $this->db->query($sqlDeleteColumn);

						// fclose($file);
						
						//creation de function
						function isFloat($var){
							if(!is_scalar($var)){
								return false;
							}
							return is_float($var+0);
						}

						function formatMoney($number, $cents = 1) { // cents: 0=never, 1=if needed, 2=always
							if (is_numeric($number)) { // a number
							  if (!$number) { // zero
								$money = ($cents == 2 ? '0.00' : '0'); // output zero
							  } else { // value
								if (floor($number) == $number) { // whole number
								  $money = number_format($number); // format
								} else { // cents
								  $money = number_format(round($number), ($cents == 0 ? 0 : 2)); // format
								} // integer or decimal
							  } // value
							  return $money;
							} // numeric
						}

						function number_format_drop_zero_decimals($n, $n_decimals){
							return ((floor($n) == round($n, $n_decimals)) ? number_format($n) : number_format($n, $n_decimals));
						}
						
						$inputFileType = PhpOffice\PhpSpreadsheet\IOFactory::identify($fileNames);
		  
						$reader = PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
						$reader->setReadDataOnly(false);
						$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileNames);
						
						$worksheet = $spreadsheet->getActiveSheet();

						//begin foreach
						foreach ($worksheet->getRowIterator() as $pCoordinate) {
							$cellIterator = $pCoordinate->getCellIterator();
							$cellIterator->setIterateOnlyExistingCells(FALSE);
							$cells = [];
							foreach ($cellIterator as $cell) {
								// $pCell =$worksheet->getCell($pCoordinate);
								$isDateTime = Date::isDateTime($cell);
								
								if($isDateTime){
									$cells[] = \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($cell->getValue(), 'DD/MM/YYYY');
									// print_r($cells);
								}elseif(is_numeric($cell->getValue())){
									if(isFloat($cell->getValue())){
										//print_r($va.'<br/>');	
										// print_r(formatMoney($va, 0).'<br/>');				  
										$okk = number_format_drop_zero_decimals($cell->getValue(), 0);
										$nombre = str_replace(',', '', $okk);										  
										//$vv = formatMoney($va, 0); 
										// print_r($nombre->getValue().'<br/>');
										// $cells[] = $nombre;
										array_push($cells,$nombre);
									}
								}else{
									$cells[] = $cell->getValue();
								}	
							}
							//print_r($cells);
							$escaped_values = array_map(null,array_values($cells));
							// $escaped_values = array_map(null,array_values($cellss));
							$txtx = array();
							$txtx = str_replace(array("'"), " ",$escaped_values);

							$newdata = "'" . implode("','", $txtx) . "'";
							var_dump($newdata);

							//si le fichier n'est pas vide
							if(!empty($newdata)){
								try{
									//création de la table
									$sqlInsertData = "INSERT INTO $table[0]$txt$tim  VALUES ($newdata)";
									// echo $sqlCreateTable;
									$stmtInsertColumn = $this->db->query($sqlInsertData);

								} catch(PDOException $e){
									echo $sqlInsertData. "<br/>" . $e->getMessage();
								}
							}
						}
						//end foreach

						// $return = $this->mdi->insert_json_in_db($tab,$json_data);
						if($stmtInsertColumn == 1){
							$data['result_msg'] = "Data successfuly inserted";
						}else{
							$data['result_msg'] = "Please configure your database correctly";
						}
						

						// Delete first row of table
						// $sqlDeleteFirstRow = "DELETE TOP(1) FROM $table[0]$txt$tim";
						$sqlDeleteFirstRow = "DELETE FROM $table[0]$txt$tim WHERE id = 1 ";
						// echo $sqlDeleteFirstRow;
						$stmtDeleteFirstRow = $this->db->query($sqlDeleteFirstRow);
						// $stmtDeleteFirstRow = $this->db->query($sqlDeleteFirstRow);
					}
				}
			}
			
			$this->_render_page('' . DIRECTORY_SEPARATOR . 'add_mdi', $data);
			//$this->load->view('add_mdi',$data);
		}
	}
	
	


	// create xlsx
    public function createXLS() {

    	$user = $this->ion_auth->user()->row();
		
		$this->data['title'] = $this->lang->line('index_heading');
		
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		//list the users
		$this->data['users'] = $this->ion_auth->users()->result();

		$data['user'] = $user;
    // create file name
        $fileName = 'data-'.time().'.xlsx';  
    // load excel library
        $this->load->library('excel');
        $tp = $user->username;
        $empInfo = $this->export->employeeList($tp);
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue(); 
        // set Row
        $rowCount = 2;
        foreach ($empInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue($rowCount, $element['TABLE_NAME']);
            $rowCount++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        //$objWriter->save(ROOT_UPLOAD_IMPORT_PATH.$fileName);
    // download file
        header("Content-Type: application/vnd.ms-excel charset=utf-8");
        header('Content-Disposition: attachment;filename="Employee Data.xlsx"');
        $objWriter->save('php://output');
        //redirect(HTTP_UPLOAD_IMPORT_PATH.$fileName);        
    }


	public function save()
	{
		if($this->input->post('importchamp')){
			$conn = mysqli_connect("localhost", "root", "", "codeigniter");

			// $fileNames = $this->input->post('input_data[tab]') ;
			// $chk_ext = explode(".",$fileNames);
			// $table = $input_data['tab']; 
	
			$input_data = $this->input->post('input_val');
			$ligne1=json_encode($input_data);
			$table = $this->input->post('input_table[tab]'); 
			$array=array($input_data);
			$ar=explode(";",$ligne1);

			
			//
			$arrays = array(0=>$ar[0]);
			$ar[0]=(isset($ar[0]) ) ? $ar[0] : Null;
			$champs1=explode(";",$ar[0]);
			
			// echo $ligne1;
			//
			$result = [];
			if(!empty($ar)){
				foreach ($ar as $key => $val) {
					$val1 = $ar[$key];

					$result[]=$val;
				
					$escaped_values = array_map(array($conn, 'real_escape_string'),array_values($result));
					$values  = implode('","', $escaped_values);
					$newdata = "'" . implode("','", $escaped_values) . "'";
					$champ_column = "'" . implode("','", $ar) . "'";

					// $quit = $input_data['quit'];
					// $mont = $input_data['mont'];
					// $debperiode = $input_data['debperiode'];
					// $finperiode = $input_data['finperiode'];

					// $tab_details = array('quit'=> $quit, 
					// 					'mont' => $mont, 
					// 					'debperiode' => $debperiode,
					// 					'finperiode' => $finperiode,
					// 				);

					foreach ($input_data as $key => $value) {
							//echo $value.',';
							$query = $this->db->query("ALTER TABLE $table ADD $value VARCHAR(1000) NOT NULL");
							//echo $query;
					}
					// var_dump($input_data);
					//alter table
					// $query = $this->db->query("ALTER TABLE $table ADD $quit VARCHAR(1000) NOT NULL");
					// echo $data['req'];
					// echo $query;
					
					// $sql1 = mysqli_query($conn,'ALTER TABLE '.$table.' ADD '.$val1.' VARCHAR(1000) NOT NULL ');
					// echo $sql1;
					// $conn->query($sql1);
				}
			}
			// die();
			//input_data['emp_pass'] = md5($this->input->post('emp_pass'));
			// $is_inserted = $this->don_model->new_donnees_query($input_data);
			
			//die();
			if($query)
				$this->session->set_flashdata('success','Le(s) Champ(s) ajouté(s) avec successfully.'); 
			else
				$this->session->set_flashdata('error','Vous ne pouvez pas ajouté le(s) champ(s)...!');
		}else{
			echo "No request...";
		}
		redirect(site_url('Mdidata/add_new'));
	}

	public function export_data(){

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}else {

			$user = $this->ion_auth->user()->row();
		
			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();

			$data['user'] = $user;

			//affiche la liste des table
			//$data['tbl_name'] = $this->mdi->get_all_table_query($user->first_name . '' . $user->last_name);
			//die();


			// $config['base_url'] = base_url().'Mdidata/export_data';
			// $config['total_rows'] = 200;
			// $config['per_page'] = 20;

			// $this->pagination->initialize($config);

			// $data['pagination'] = $this->pagination->create_links();
			// $data['tuples'] = $this->mdi->get_table_from_user_query();
			
			// $config['base_url'] = base_url().'Mdidata/export_data';        
			// $config['total_rows'] = $this->mdi->get_count_sondage_query($table[0]);
			
			// $config['per_page'] = 50;        
			// $config['uri_segment'] = 3;        
			// $config['use_page_numbers'] = TRUE;        
			// $config['full_tag_open'] = '<ul class="pagination">';        
			// $config['full_tag_close'] = '</ul>';  
			// $config['attributes'] = array('class' => 'page-link');   
			// $config['first_link'] = 'First';        
			// $config['last_link'] = 'Last';   
			// $config['first_tag_open'] = '<li>';        
			// $config['first_tag_close'] = '</li>';        
			// $config['prev_link'] = '&laquo';        
			// $config['prev_tag_open'] = '<li class="prev">';        
			// $config['prev_tag_close'] = '</li>';        
			// $config['next_link'] = '&raquo';        
			// $config['next_tag_open'] = '<li>';        
			// $config['next_tag_close'] = '</li>';        
			// $config['last_tag_open'] = '<li>';        
			// $config['last_tag_close'] = '</li>';        
			// $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';        
			// $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';        
			// $config['num_tag_open'] = '<li>';
			// $config['num_tag_close'] = '</li>';
			// $config['num_links'] = 1;

			
			// $this->pagination->initialize($config);   
			// // $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			// $page = $this->uri->segment(3);
			// $start = ($page - 0) * $config["per_page"];
			// $data['pagination'] = $this->pagination->create_links();
			$ok = $user->username;
			$data['tuples'] = $this->umodel->get_all_table_query($ok);


			$this->_render_page('' . DIRECTORY_SEPARATOR . 'alldata', $data);
			// $this->load->view('datatable',$data);
		}

	}
	
	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
}

?>