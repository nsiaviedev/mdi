<?php defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('display_errors', 'on');
// phpinfo();
ini_set('max_execution_time',0);

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
 //Load plugin
require (APPPATH .'third_party/vendor/autoload.php');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use PhpOffice\PhpSpreadsheet\Shared\Date;

require_once(APPPATH .'libraries/PHPExcel.php');


class PrelevementController extends CI_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('excel');
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);
		$this->load->model('Mdi_model','mdi');
		$this->load->model('Export_model', 'export');
		$this->load->model('Table_model', 'umodel');
		$this->load->library('pagination');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
	}
	
	public function index() {
	
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			//show_error('You must be an administrator to view this page.');
			redirect('PrelevementController/add_prelevement', 'refresh');
		}
		else
		{
			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();

			$user = $this->ion_auth->user()->row();
			$data['user'] = $user;
			
			//USAGE NOTE - you can do more complicated queries like this
			//$this->data['users'] = $this->ion_auth->where('field', 'value')->users()->result();
			
			// foreach ($this->data['users'] as $k => $user)
			// {
				// $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			// }

			$this->_render_page('' . DIRECTORY_SEPARATOR . 'index', $data);
		}
	}


	public function search($dated = 20240301/*,$datef = 0*/){

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}else {

			$user = $this->ion_auth->user()->row();
		
			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();

			$data['user'] = $user;

			//affiche la liste des table
			//$data['tbl_name'] = $this->mdi->get_all_table_query($user->first_name . '' . $user->last_name);
			//die();

			if(isset($_POST['ch_periode']))
			{
				if($_POST['datedebut']!= 0 /*AND $_POST['datefin']!= "" */)
				{
					$dated = $_POST['datedebut'];
					//$datef = $_POST['datefin'];
				} 
			}

			$date_d = $dated;
			$data_querry = "EXEC [dbo].[detail_envoi_prel_new] ".$date_d;
			
			//echo 'jjjjjjjjjjjjjj';
			$queryrecl=$this->db->query("SELECT * FROM OPENQUERY ([LINK_SUNSHINE],'".$data_querry."' )");

			$result = $queryrecl->result_array();

			
			$data['tuples'] = $result;
			//$data['tuples'] = $this->umodel->get_all_table_query($ok);


			$this->_render_page('' . DIRECTORY_SEPARATOR . 'search_recap_prelevement', $data);
			// $this->load->view('datatable',$data);
		}

	}

	// 05-05-2024

	public function export_csv(){ 

		// file name 
		$get_uri = $this->uri->segment(3);
		$output = '';
		$data_querry = "EXEC [dbo].[detail_envoi_prel_new] ".$get_uri;
		$query=$this->db->query("SELECT * FROM OPENQUERY ([LINK_SUNSHINE],'".$data_querry."' )");

		// $query = $this->db->query("SELECT * FROM $get_uri;");
		$result = $query->result_array();

		//echo($get_uri);
		if(isset($_POST["export"]) and $_POST['export'] == "Export xls")
		{
			//echo "ok xls";
			if(count($result) > 0)
			{
				$columns_names = array_keys($result[0]);
				//var_dump($columns_names);
				
			  $output .= '
			   <table class="table" bordered="1">
				   <thead>  
					  <tr>';
						foreach($columns_names as $col) {
					        $output .= '<th class="wd-15p">'. $col .'</th>';
					    }  
					       
					  $output .= '</tr></thead><tbody>';

						foreach($result as $tuple) {
					        $output .=  '<tr>';
					        foreach($tuple as $col) {
					            $output .=  '<td>'. $col .'</td>';
					        }
					        $output .=  '</tr>';
					    }
					  // die();
			$output .= '</tbody></table>';

			header('Content-Type: application/xls');
			header('Content-Disposition: attachment; filename=download.xls');
			echo $output;
			}

		}elseif(isset($_POST["export"]) and $_POST['export'] == "Export xlsx")
		{
			echo "ok xlsx";
			die('Le format XLSX est en cours');

		}elseif(isset($_POST["export"]) and $_POST['export'] == "Export csv")
		{
			echo "ok csv";
			die('Le format CSV est en cours');
			
			header('Content-Type: text/csv; charset=utf-8');  
		    header('Content-Disposition: attachment; filename=download.csv');  
		    $output = fopen("php://output", "w");  
		    fputcsv($output, array('ID ADRESSE', 'VILLE', 'LATITUDE', 'LONGITUDE'),";");  
		    //$query = "SELECT * from address ORDER BY id ASC";  
		    //$result = mysqli_query($connect, $query);  
		    while($row = $query->result_array())  
		    {  
		        fputcsv($output, $row,";");  
		    }  
		    fclose($output);

		}
		die();
		 
	}
	
	
	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}


	public function obaExtrait($tab_principale, $tab_secondaires, $tab_champs, $getEntetes)
	{

		$police = "NSIACIF.JAPOLIP";
		$quittance = "NSIACIF.JAQUITP";
		$agent = "NSIACIF.JAAGENP";
		$payeur = "NSIACIF.JAIDENP PAYEUR";
		$client = "NSIACIF.JAIDENP CLIENT";
		$compte_bancaire = "NSIACIF.JACORBP BANCAIRE";
		$assures = "NSIACIF.JAIDENP ASSURES";

		$id_police = "NUMERO_POLICE";
		$id_quittance = "NUMERO_QUITTANCE";
		$aggregation_1 ="MAX";
		$aggregation_2 ="COUNT";

		$id_agent=$id_identite="";
		$filtre=[];
		//$grouper=['(MAX)','(COUNT)','SDSD'];

		$query2 = $this->db->query("SELECT * FROM $tab_principale");
 		$result2 = $query2->result_array();
 		$oook = array_keys($result2[0]);
 		


 		
 		$the_array = array_merge($oook,$tab_champs);

		$tab_champ =  ( (count($the_array)>1) ? implode(',', $the_array) : $the_array[0] ) ;
		// $tab_champ_p =  ( (count($oook)>1) ? implode(',', $oook) : $oook[0] ) ;
		//$getBcivietable = "[LINK_BCIVIE].[B_CIVIE].[DBO]";
        
		$oba_extraction = "SELECT ".$tab_champ." FROM [LINK_BCIVIE].[B_CIVIE].[DBO].".$tab_principale." A ";

		
		foreach ($tab_secondaires as $keys => $tab_secondaire) {
			
			if (strpos($tab_secondaire, $police) !== false) {
				$filtre[] = " JAPOLIP_WNUPO>0";

				if (array_search($id_police, $getEntetes)!==false) {
										

					$tab_secondaire_reecrit_1 = str_replace("%numeropo%", "NUMERO_POLICE", $tab_secondaire);
					$tab_secondaire_reecrit_2 = str_replace("%idbdpo%", "POLICE.JAPOLIP_WNUPO", $tab_secondaire_reecrit_1);

					$oba_extraction .= " ".$tab_secondaire_reecrit_2;
				}else {
					// var_dump($getEntetes);
					// die('ok nooooooooooo');
					$tab_secondaire_reecrit_1 = str_replace("%numeropo%", "(SELECT TOP 1 WNUPO FROM NSIACIF.JAQUITP WHERE MPYCO>0 AND NUMERO_QUITTANCE = WNUCO)", $tab_secondaire);
					$tab_secondaire_reecrit_2 = str_replace("%idbdpo%", "POLICE.JAPOLIP_WNUPO", $tab_secondaire_reecrit_1);

					$oba_extraction .= " ".$tab_secondaire_reecrit_2;
				}
				
		        //$oba_extraction .= "".$tab_secondaire;
			}

			if (strpos($tab_secondaire, $quittance) !== false) {

				$filtre[] = "MPYCO>0";

				if (array_search($id_quittance, $getEntetes)!==false) {
					
					

					$tab_secondaire_reecrit_1 = str_replace("%numero%", "NUMERO_QUITTANCE", $tab_secondaire);
					$tab_secondaire_reecrit_2 = str_replace("%idbd%", "WNUCO", $tab_secondaire_reecrit_1);

					$oba_extraction .= " ".$tab_secondaire_reecrit_2;
				}else {
					// var_dump($getEntetes);
					// die('ok nooooooooooo');
					$tab_secondaire_reecrit_1 = str_replace("%numero%", "NUMERO_POLICE", $tab_secondaire);
					$tab_secondaire_reecrit_2 = str_replace("%idbd%", "WNUPO", $tab_secondaire_reecrit_1);

					$oba_extraction .= " ".$tab_secondaire_reecrit_2;
				}
			}
			if (strpos($tab_secondaire, $agent) !== false) {
								
		        $oba_extraction .= " ".$tab_secondaire;
			} 

			if (strpos($tab_secondaire, $payeur) !== false) {
								
		        $oba_extraction .= " ".$tab_secondaire;
			}
			if (strpos($tab_secondaire, $client) !== false) {
								
		        $oba_extraction .= " ".$tab_secondaire;
			}
			if (strpos($tab_secondaire, $compte_bancaire) !== false) {
								
		        $oba_extraction .= " ".$tab_secondaire;
			}

			if (strpos($tab_secondaire, $assures) !== false) {
								
		        $oba_extraction .= " ".$tab_secondaire;
			}

		}


			
		if (!empty($filtre)) {

				$oba_extraction .= " WHERE ".( (count($filtre)>1) ? implode(' AND ', $filtre) : $filtre[0] );
		}

		

	     $deleteKeys=$setGroupby = array();
		 $agregats = array('COUNT','MAX');
		 $the_keys = '';

			// parcours des données
			foreach($the_array as $key => $champ)
			{
				 $the_keys = $key;
				 $setGroupby[] = $champ;
			  //parcours des mots-clés
			  foreach($agregats as $search)
			  {
			    // stockage des résultats positifs
			    if(strpos($champ,$search)!==false)
			    {
			        $deleteKeys[] = $the_keys;
			    }

			  }
			}

			foreach ($setGroupby as $key => $setGroup) {

				foreach($deleteKeys as $deleteKey){

				    // stockage des résultats positifs
				    if( $key == $deleteKey )
			    	{
			        	// $deleteKeys[] = $the_keys;
			        	unset($setGroupby[$key]);
			    	}
				}
			}

			
			$newSetGroupBy = array();


			foreach ($setGroupby as $key => $setGB) {

				if (strpos($setGB, 'CASE') !== false) {
					$newSetGroupBy[] = 'BANCAIRE.JACORBP_BANXD';
				}elseif (strpos($setGB, 'AS ') !== false) {

					$newSetGroupBy[] = substr($setGB,0,strpos($setGB, "AS "));
				}else{
					$newSetGroupBy[] = $setGB;
				}
				
			}
		    // var_dump($newSetGroupBy);
		    // die('resultat');
			$oba_extraction .= " GROUP BY ".( (count($newSetGroupBy)>1 ) ? implode(' , ', $newSetGroupBy) : $newSetGroupBy[0]);

			$obaExtraitTout = "SELECT DISTINCT * FROM OPENQUERY ([LINK_SUNSHINE],'".$oba_extraction."')";

				


		 $requete =  $this->db->query($obaExtraitTout);

		 // $result = $requete->fetchAll(PDO::FETCH_ASSOC);


		$user = $this->ion_auth->user()->row();
	
		$this->data['title'] = $this->lang->line('index_heading');
		
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		//list the users
		$this->data['users'] = $this->ion_auth->users()->result();

		$data['user'] = $user;

		$tim = time();//recupère dans $tim le timestamp
		$txt = preg_replace('/\s/','',$user->first_name.''.$user->last_name);//supprime les espaces


		$newtable = $txt.$tim;


		//$data['oo'] = $result;

		//$jso = json_encode($result);
		//echo "<br/>";

		// $sqlCreateTable = "CREATE TABLE $newtable (idnew VARCHAR(MAX) )";
			// $sqlCreateTable = "CREATE TABLE $table[0]$txt$tim (tempon VARCHAR(1000) )";
			//echo $sqlCreateTable;
			// $stmtCreateTable = $this->db->query($sqlCreateTable);

		//Insertion dans la nouvelle table cree
		// $sqlInsertData = "INSERT INTO $newtable(idnew) VALUES ('$jso')";
		//echo $sqlInsertData;
		// die();
		// $stmtInsertColumn = $this->db->query($sqlInsertData);
		
		//test de la nouvelle requete
		
		$req1 = "SELECT * INTO $newtable FROM ($obaExtraitTout)T ";
		
		$stmtInsertColumn1 = $this->db->query($req1);
		

		//$this->load->view('tabletest',$data);

		// var_dump($stmtInsertColumn1);

		if(isset($stmtInsertColumn1)){
			redirect('Mdidata/export_data');
		}else{
			die("redirect is not possible");
		}
 
		
	}


	

	
}

?>