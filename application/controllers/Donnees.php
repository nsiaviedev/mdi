<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donnees extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		// Load session
		$this->load->library('session');
		//connection to model sondage
		// $this->load->model('Sondage_model', 'emp_model', TRUE);
		$this->load->model('Donnees_model', 'don_model');
		$this->load->model('Params_model', 'para_model', TRUE);
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->library('pagination');
		$this->load->helper(array('url','language'));
		
		// Load model
		// $this->load->model('Main_model');

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	public function index($data = [])
	{
		if (!$this->ion_auth->logged_in())
		{
			$url_actuellement = uri_string() ;
			$this->session->set_userdata('last_url', $url_actuellement);
			redirect('auth/login');

		}else{
			//var_dump($_SESSION) ;

			// $user = $this->ion_auth->user()->row();

			// $data['user'] = $user;
			// $data['titre'] = "BIENVENUE DANS VOTRE OUTIL FINANCIER - Joseph & Daniel - Advisory" ;

			// $module = $this->uri->segment(2) ;

			// $data['active'] = $module ;
			
			
			
			$this->load->view('template/header',$data);
			// $data['donnees'] = $this->don_model->get_all_donnees_query();
			$config['base_url'] = base_url().'donnees/index/';        
			$config['total_rows'] = $this->don_model->get_count_sondage_query();
			
			$config['per_page'] = 50;        
			$config['uri_segment'] = 3;        
			$config['use_page_numbers'] = TRUE;        
			$config['full_tag_open'] = '<ul class="pagination">';        
			$config['full_tag_close'] = '</ul>';  
			$config['attributes'] = array('class' => 'page-link');   
			$config['first_link'] = 'First';        
			$config['last_link'] = 'Last';   
			$config['first_tag_open'] = '<li>';        
			$config['first_tag_close'] = '</li>';        
			$config['prev_link'] = '&laquo';        
			$config['prev_tag_open'] = '<li class="prev">';        
			$config['prev_tag_close'] = '</li>';        
			$config['next_link'] = '&raquo';        
			$config['next_tag_open'] = '<li>';        
			$config['next_tag_close'] = '</li>';        
			$config['last_tag_open'] = '<li>';        
			$config['last_tag_close'] = '</li>';        
			$config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';        
			$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';        
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['num_links'] = 1;

			
			$this->pagination->initialize($config);   
			// $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$page = $this->uri->segment(3);
			$start = ($page - 0) * $config["per_page"];
			$datas['pagination'] = $this->pagination->create_links();     
			$datas['donnees'] = $this->don_model->get_sondage_pagination_query($config["per_page"], $start); 
			
			$this->load->view('template/menu',$data);
			$this->load->view('home_donnees',$datas);
			
			// $data['url'] = $this->uri->segment(1) ;
			$this->load->view('template/footer', $data);
		}
		
		
	}
	
	public function add_new($data = [])
	{
		if (!$this->ion_auth->logged_in())
		{
			$url_actuellement = uri_string() ;
			$this->session->set_userdata('last_url', $url_actuellement);
			redirect('auth/login');

		}else{
			
			$this->load->view('template/header',$data);
			$this->load->view('template/menu',$data);
			$data['projet'] = $this->para_model->get_all_projets_query();
			$data['occupation'] = $this->para_model->get_all_occupations_query();
			$data['zone'] = $this->para_model->get_all_zones_query();
			$this->load->view('add_new_donnees',$data);
			$this->load->view('template/footer',$data);
		}
	}
	
	public function search( $rowno=0, $data = [] )
	{
		if (!$this->ion_auth->logged_in())
		{
			$url_actuellement = uri_string() ;
			$this->session->set_userdata('last_url', $url_actuellement);
			redirect('auth/login');

		}else{
			
			$this->load->view('template/header');
			$this->load->view('template/menu');
			// $key = $this->input->post('search[data]');
			
			// Search text
			$search_text = "";
			if($this->input->post('submit') != NULL ){
			  $search_text = $this->input->post('search');
			  $this->session->set_userdata(array("search"=>$search_text));
			}else{
			  if($this->session->userdata('search') != NULL){
				$search_text = $this->session->userdata('search');
			  }
			}
			// Row per page
			$rowperpage = 50;

			// Row position
			if($rowno != 0){
			  $rowno = ($rowno-1) * $rowperpage;
			}
			
			// All records count
			$allcount = $this->don_model->getrecordCount($search_text);

			// Get records
			$users_record = $this->don_model->getData($rowno,$rowperpage,$search_text);
			
			$config['base_url'] = base_url("donnees/search/$search_text");        
			$config['total_rows'] = $allcount;
			
			$config['per_page'] = $rowperpage;       
			$config['use_page_numbers'] = TRUE;        
			$config['full_tag_open'] = '<ul class="pagination">';        
			$config['full_tag_close'] = '</ul>';  
			$config['attributes'] = array('class' => 'page-link');   
			$config['first_link'] = 'First';        
			$config['last_link'] = 'Last';   
			$config['first_tag_open'] = '<li>';        
			$config['uri_segment'] = 4;        
			$config['first_tag_close'] = '</li>';        
			$config['prev_link'] = '&laquo';        
			$config['prev_tag_open'] = '<li class="prev">';        
			$config['prev_tag_close'] = '</li>';        
			$config['next_link'] = '&raquo';        
			$config['next_tag_open'] = '<li>';        
			$config['next_tag_close'] = '</li>';        
			$config['last_tag_open'] = '<li>';        
			$config['last_tag_close'] = '</li>';        
			$config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';        
			$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';        
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			// $config['num_links'] = 1;
			
			$this->pagination->initialize($config);   
			// $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			// $page = $config['uri_segment'];
			// $start = ($page + 1) * $config["per_page"];
			// $datas['pagination'] = $this->pagination->create_links();     
			// $results = $this->don_model->get_data_search_pagination_query($key, $config["per_page"], $start); 
			
			// $data['results'] = $this->don_model->search($key);
			
			$datas['pagination'] = $this->pagination->create_links();
			$datas['result'] = $users_record;
			$datas['row'] = $rowno;
			$datas['search'] = $search_text;
			
			// $input_data = $this->input->post('input_val');
			// var_dump($input_data);
			// var_dump($input_data['search']);
			// die();
			
			$this->load->view('recherche',$datas);
			$this->load->view('template/footer');
		}
	}
	
	
	public function skeyword( $rowno = 0)
	{
		if (!$this->ion_auth->logged_in())
		{
			$url_actuellement = uri_string() ;
			$this->session->set_userdata('last_url', $url_actuellement);
			redirect('auth/login');

		}else{
			
			// Search text
			$search_text = "";
			if($this->input->post('search') != NULL ){
			  $search_text = $this->input->post('search');
			  $this->session->set_userdata(array("search"=>$search_text));
			}else{
			  if($this->session->userdata('search') != NULL){
				$search_text = $this->session->userdata('search');
			  }
			}

			// Row per page
			$rowperpage = 500;

			// Row position
			if($rowno != 0){
			  $rowno = ($rowno-1) * $rowperpage;
			}
		 
			// All records count
			$allcount = $this->don_model->getrecordCount($search_text);

			// Get records
			$users_record = $this->don_model->getData($rowno,$rowperpage,$search_text);
		 
			// Pagination Configuration
			$config['base_url'] = base_url().'index.php/donnees/skeyword/';
			$config['use_page_numbers'] = TRUE;
			$config['total_rows'] = $allcount;
			$config['per_page'] = $rowperpage;

			// Initialize
			$this->pagination->initialize($config);
		 
			$data['pagination'] = $this->pagination->create_links();
			$data['result'] = $users_record;
			$data['row'] = $rowno;
			$data['search'] = $search_text;
			

			// Load view
			$this->load->view('template/header');
			$this->load->view('template/menu');
			$this->load->view('search',$data);
			$this->load->view('template/footer');
		}
	}
	
	
	public function save()
	{
		if($this->input->post('btn_save')){
			$input_data = $this->input->post('input_val');
			//input_data['emp_pass'] = md5($this->input->post('emp_pass'));
			$is_inserted = $this->don_model->new_donnees_query($input_data);
			// var_dump($input_data);
			//die();
			if($is_inserted)
				$this->session->set_flashdata('success','New Donnée has been inserted successfully.'); 
			else
				$this->session->set_flashdata('error','You could not insert new donnée...!');
		}else{
			echo "No request...";
		}
		redirect(base_url('donnees/add_new'));
	}
	
	public function edit($id = false)
	{
		$this->load->view('template/header');
		if(is_numeric($id))
			$sond_id = $id;
		else
			redirect(base_url('sondage/'));
		$data['sondage'] = $this->don_model->get_sondage_by_Id_query($sond_id);
		$this->load->view('edit_sondage', $data);
		$this->load->view('template/footer');
	}
	
	public function view($id = false)
	{
		$this->load->view('template/header');
		if(is_numeric($id))
			$sond_id = $id;
		else
			redirect(base_url('sondage/'));
		$data['sondage'] = $this->emp_model->get_sondage_by_Id_query($sond_id);
		$this->load->view('edit_sondage', $data);
		$this->load->view('template/footer');
	}
	
	public function update()
	{
		$sondage_id = $this->input->post('sondage_id');
		if($this->input->post('btn_save')){
			$input_data = $this->input->post('input_val');
			$is_updated = $this->emp_model->update_sondage_query($sondage_id, $input_data);
			if($is_updated)
				$this->session->set_flashdata('success','Sondage '.$sondage_id.' update successfully.'); 
			else
				$this->session->set_flashdata('error','You could not update sondage...!');
		}else{
			$this->session->set_flashdata('error','No request...!');
		}
		redirect(base_url().'sondage/edit/'.$sondage_id);
	}
	
	public function delete_data()
	{
		$data_id = $this->input->post('data_id');
		if($this->input->post('btn_save')){
			$input_data = $this->input->post('input_val');
			$is_updated = $this->emp_model->delete_data_query($data_id, $input_data);
			if($is_updated)
				$this->session->set_flashdata('success','Sondage '.$data_id.' update successfully.'); 
			else
				$this->session->set_flashdata('error','You could not update sondage...!');
		}else{
			$this->session->set_flashdata('error','No request...!');
		}
		redirect(base_url().'sondage/edit/'.$data_id);
	}
}
