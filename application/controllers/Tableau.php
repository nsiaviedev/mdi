<?php defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('display_errors', 'on');
// phpinfo();
ini_set('max_execution_time',0);

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
 //Load plugin
require (APPPATH .'third_party/vendor/autoload.php');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class Tableau extends CI_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('excel');
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);
		$this->load->model('Mdi_model','mdi');
		$this->load->model('Export_model', 'export');
		$this->load->model('Table_model', 'umodel');
		$this->load->library('pagination');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		// Per page limit 
        $this->perPage = 10; 
	}
	
	public function index() {
	
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			//show_error('You must be an administrator to view this page.');
			redirect('Mdidata/add_new', 'refresh');
		}
		else
		{
			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();

			$user = $this->ion_auth->user()->row();
			$data['user'] = $user;
			
			//USAGE NOTE - you can do more complicated queries like this
			//$this->data['users'] = $this->ion_auth->where('field', 'value')->users()->result();
			
			// foreach ($this->data['users'] as $k => $user)
			// {
				// $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			// }

			$this->_render_page('' . DIRECTORY_SEPARATOR . 'index', $data);
		}
	}
	
	public function liste($name = false)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}else {
			
			$user = $this->ion_auth->user()->row();
		
			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();

			$data['user'] = $user;

			$t_name = $name;
			$data['tt'] = $t_name;

			// init params
			$conditions = array(); 
        	$uriSegment = 4 ;

			 // Get record count 
			 $conditions['returnType'] = 'count'; 
			 $totalRec = $this->mdi->getRows($conditions, $t_name);

			// Pagination configuration 
			$config['base_url']    = base_url().'Tableau/liste/'.$t_name.'/'; 
			$config['uri_segment'] = $uriSegment; 
			$config['total_rows']  = $totalRec; 
			$config['per_page']    = $this->perPage;

			// Pagination link format 
			$config['full_tag_open'] = '<ul class="pagination">';        
			$config['full_tag_close'] = '</ul>';  
			$config['attributes'] = array('class' => 'page-link');   
			$config['first_link'] = 'First';        
			$config['last_link'] = 'Last';   
			$config['first_tag_open'] = '<li>';        
			$config['first_tag_close'] = '</li>';        
			$config['prev_link'] = '&laquo';        
			$config['prev_tag_open'] = '<li class="prev">';        
			$config['prev_tag_close'] = '</li>';        
			$config['next_link'] = '&raquo';        
			$config['next_tag_open'] = '<li>';        
			$config['next_tag_close'] = '</li>';        
			$config['last_tag_open'] = '<li>';        
			$config['last_tag_close'] = '</li>';        
			$config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';        
			$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';        
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			// Initialize pagination library 
			$this->pagination->initialize($config); 
         
			// Define offset 
			$page = $this->uri->segment($uriSegment); 
			$offset = !$page?0:$page; 
			 
			// Get records 
			$conditions = array( 
				'start' => $offset, 
				'limit' => $this->perPage 
			);

			$data['tuples'] = $this->mdi->getRows($conditions,$t_name); 

			$this->_render_page('' . DIRECTORY_SEPARATOR . 'newtable', $data);
			//$this->load->view('add_mdi',$data);
		}
	}

	public function export_csv(){ 
		// file name 
		$get_uri = $this->uri->segment(3);
		$output = '';
		$query = $this->db->query("SELECT * FROM $get_uri;");
		$result = $query->result_array();

		//echo($get_uri);
		if(isset($_POST["export"]) and $_POST['export'] == "Export xls")
		{
			//echo "ok xls";
			if(count($result) > 0)
			{
				$columns_names = array_keys($result[0]);
				//var_dump($columns_names);
				
			  $output .= '
			   <table class="table" bordered="1">
				   <thead>  
					  <tr>';
						foreach($columns_names as $col) {
					        $output .= '<th class="wd-15p">'. $col .'</th>';
					    }  
					       
					  $output .= '</tr></thead><tbody>';

						foreach($result as $tuple) {
					        $output .=  '<tr>';
					        foreach($tuple as $col) {
					            $output .=  '<td>'. $col .'</td>';
					        }
					        $output .=  '</tr>';
					    }
					  // die();
			$output .= '</tbody></table>';

			header('Content-Type: application/xls');
			header('Content-Disposition: attachment; filename=download.xls');
			echo $output;
			}

		}elseif(isset($_POST["export"]) and $_POST['export'] == "Export xlsx")
		{
			echo "ok xlsx";
			

		}elseif(isset($_POST["export"]) and $_POST['export'] == "Export csv")
		{
			echo "ok csv";
			
			header('Content-Type: text/csv; charset=utf-8');  
		    header('Content-Disposition: attachment; filename=download.csv');  
		    $output = fopen("php://output", "w");  
		    fputcsv($output, array('ID ADRESSE', 'VILLE', 'LATITUDE', 'LONGITUDE'),";");  
		    //$query = "SELECT * from address ORDER BY id ASC";  
		    //$result = mysqli_query($connect, $query);  
		    while($row = $query->result_array())  
		    {  
		        fputcsv($output, $row,";");  
		    }  
		    fclose($output);

		}
		die();
		 
	}
	
	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
}

?>