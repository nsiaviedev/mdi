<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Params extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		//connection to model sondage
		$this->load->model('Params_model', 'para_model', TRUE);
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->library('pagination');
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	public function index($data = [], $rowno=0, $rownosexe=0)
	{
		if (!$this->ion_auth->logged_in())
		{
			$url_actuellement = uri_string() ;
			$this->session->set_userdata('last_url', $url_actuellement);
			redirect('auth/login');

		}else{

			//var_dump($_SESSION) ;

			// $user = $this->ion_auth->user()->row();

			// $data['user'] = $user;
			// $data['titre'] = "BIENVENUE DANS VOTRE OUTIL FINANCIER - Joseph & Daniel - Advisory" ;

			// $module = $this->uri->segment(2) ;

			// $data['active'] = $module ;
			
			
			$this->load->view('template/header',$data);			
			
			// Row per page
			$rowperpage = 100;

			// Row position
			if($rowno != 0){
			  $rowno = ($rowno-1) * $rowperpage;
			}
			
			// All records count
			$allcount = $this->para_model->getrecordCount();
			// var_dump($allcount);
			// die();

			// Get records
			$users_record = $this->para_model->getData($rowno,$rowperpage);
			
			//Statistiques du nombre de personnes par projets et par zones
			$config['base_url'] = base_url("index.php/params/index");        
			$config['total_rows'] = $allcount;
			
			$config['per_page'] = $rowperpage;       
			$config['use_page_numbers'] = TRUE;        
			$config['full_tag_open'] = '<ul class="pagination">';        
			$config['full_tag_close'] = '</ul>';  
			$config['attributes'] = array('class' => 'page-link');   
			$config['first_link'] = 'First';        
			$config['last_link'] = 'Last';   
			$config['first_tag_open'] = '<li>';        
			$config['uri_segment'] = 3;        
			$config['first_tag_close'] = '</li>';        
			$config['prev_link'] = '&laquo';        
			$config['prev_tag_open'] = '<li class="prev">';        
			$config['prev_tag_close'] = '</li>';        
			$config['next_link'] = '&raquo';        
			$config['next_tag_open'] = '<li>';        
			$config['next_tag_close'] = '</li>';        
			$config['last_tag_open'] = '<li>';        
			$config['last_tag_close'] = '</li>';        
			$config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';        
			$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';        
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['num_links'] = 1;
			
			$this->pagination->initialize($config);
			
			$datas['pagination'] = $this->pagination->create_links();
			$datas['result'] = $users_record;
			$datas['row'] = $rowno;
			
			
			$this->load->view('template/menu',$data);
			$this->load->view('datadash',$datas);
			
			// $data['url'] = $this->uri->segment(1) ;
			$this->load->view('template/footer', $data);
		}
		
		
	}
	
	public function projet($data = [])
	{
		if (!$this->ion_auth->logged_in())
		{
			$url_actuellement = uri_string() ;
			$this->session->set_userdata('last_url', $url_actuellement);
			redirect('auth/login');

		}else{
			
			$this->load->view('template/header',$data);
			$this->load->view('template/menu',$data);
			$data['projet'] = $this->para_model->get_all_projets_query();
			$this->load->view('projet',$data);
			$this->load->view('template/footer',$data);
		}
	}
	
	public function occupation($data = [])
	{
		if (!$this->ion_auth->logged_in())
		{
			$url_actuellement = uri_string() ;
			$this->session->set_userdata('last_url', $url_actuellement);
			redirect('auth/login');

		}else{
			
			$this->load->view('template/header',$data);
			$this->load->view('template/menu',$data);
			$data['occupation'] = $this->para_model->get_all_occupations_query();
			$this->load->view('occupation',$data);
			$this->load->view('template/footer',$data);
		}
	}
	
	public function zone($data = [])
	{
		if (!$this->ion_auth->logged_in())
		{
			$url_actuellement = uri_string() ;
			$this->session->set_userdata('last_url', $url_actuellement);
			redirect('auth/login');

		}else{
			
			$this->load->view('template/header',$data);
			$this->load->view('template/menu',$data);
			$data['zone'] = $this->para_model->get_all_zones_query();
			$this->load->view('zone',$data);
			$this->load->view('template/footer',$data);
		}
	}
	
	public function save_projet()
	{
		if($this->input->post('btn_save')){
			$input_data = $this->input->post('input_val');
			//input_data['emp_pass'] = md5($this->input->post('emp_pass'));
			$is_inserted = $this->para_model->new_projet_query($input_data);
			var_dump($input_data);
			// die();
			if($is_inserted)
				$this->session->set_flashdata('success','New projet has been inserted successfully.'); 
			else
				$this->session->set_flashdata('error','You could not insert new projet...!');
		}else{
			echo "No request...";
		}
		redirect(base_url('params/projet'));
	}
	
	public function save_occupation()
	{
		if($this->input->post('btn_save')){
			$input_data = $this->input->post('input_val');
			//input_data['emp_pass'] = md5($this->input->post('emp_pass'));
			$is_inserted = $this->para_model->new_occupation_query($input_data);
			var_dump($input_data);
			// die();
			if($is_inserted)
				$this->session->set_flashdata('success','New occupation has been inserted successfully.'); 
			else
				$this->session->set_flashdata('error','You could not insert new occupation...!');
		}else{
			echo "No request...";
		}
		redirect(base_url('params/occupation'));
	}
	
	public function save_zone()
	{
		if($this->input->post('btn_save')){
			$input_data = $this->input->post('input_val');
			//input_data['emp_pass'] = md5($this->input->post('emp_pass'));
			$is_inserted = $this->para_model->new_zone_query($input_data);
			var_dump($input_data);
			// die();
			if($is_inserted)
				$this->session->set_flashdata('success','New zone has been inserted successfully.'); 
			else
				$this->session->set_flashdata('error','You could not insert new zone...!');
		}else{
			echo "No request...";
		}
		redirect(base_url('params/zone'));
	}
	
	public function edit($id = false)
	{
		$this->load->view('template/header');
		if(is_numeric($id))
			$proj_id = $id;
		else
			redirect(base_url('projet/'));
		$data['projet'] = $this->para_model->get_projet_by_Id_query($proj_id);
		$this->load->view('edit_projet', $data);
		$this->load->view('template/footer');
	}
	
	public function view($id = false)
	{
		$this->load->view('template/header');
		if(is_numeric($id))
			$proj_id = $id;
		else
			redirect(base_url('projet/'));
		$data['projet'] = $this->para_model->get_projet_by_Id_query($sond_id);
		$this->load->view('view_projet', $data);
		$this->load->view('template/footer');
	}
	
	public function update()
	{
		$sondage_id = $this->input->post('projet_id');
		if($this->input->post('btn_save')){
			$input_data = $this->input->post('input_val');
			$is_updated = $this->para_model->update_sondage_query($sondage_id, $input_data);
			if($is_updated)
				$this->session->set_flashdata('success','projet '.$sondage_id.' update successfully.'); 
			else
				$this->session->set_flashdata('error','You could not update projet...!');
		}else{
			$this->session->set_flashdata('error','No request...!');
		}
		redirect(base_url().'projet/edit/'.$sondage_id);
	}
}
