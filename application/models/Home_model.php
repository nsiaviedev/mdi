<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {
	
	public function new_sondage_query($data)
	{
		$this->db->insert('sondage',$data);
		return $insert_id = $this->db->insert_id();
	}
	
	public function get_count_typeprojet_query()
	{
		$this->db->select("COUNT(*) as num_row");
		$this->db->from('donnees');
		$this->db->where('type_donnees','');
		$query = $this->db->get();
		$result = $query->result();
		return $result[0]->num_row;
	}
	
	public function get_count_total_donnees_query()
	{
		$this->db->select("COUNT(*) as num_row");
		$this->db->from('donnees');
		$query = $this->db->get();
		$result = $query->result();
		return $result[0]->num_row;
		
	}
	
	public function get_data_par_type_projet_query()
	{
		$this->db->select('type_donnees, count(id_donnees) as nbre');
		$this->db->from('donnees');
		$this->db->group_by('type_donnees');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $result = $query->result();
	}
	
	
	public function get_data_par_residence_query()
	{
		$this->db->select('residence_donnees, count(id_donnees) as nbre');
		$this->db->from('donnees');
		$this->db->group_by('residence_donnees');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $result = $query->result();
	}
	
	public function get_data_par_zone_query()
	{
		$this->db->select('zone_donnees, count(id_donnees) as nbre');
		$this->db->from('donnees');
		$this->db->group_by('zone_donnees');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $result = $query->result();
	}
	
	public function get_data_zone_typeprojet_query()
	{
		$this->db->select('zone_donnees,type_donnees, count(id_donnees) as nbre');
		$this->db->from('donnees');
		$this->db->group_by(array('zone_donnees','type_donnees'));
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $result = $query->result();
	}
	
	public function get_all_sondage_query()
	{
		$this->db->select('*');
		$this->db->from('sondage');
		$query = $this->db->get();
		return $result = $query->result();
		//SELECT * FROM sondage
	}
	
	public function get_sondage_by_Id_query($sond_id)
	{
		$this->db->select('*');
		$this->db->from('sondage');
		$this->db->where('id_sondage',$sond_id);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $result = $query->row();
	}
	
	public function update_sondage_query($sondage_id,$data)
	{
		$this->db->where('id_sondage',$sondage_id);
		return $this->db->update('sondage',$data);
	}
	
	// Fetch records
  public function getData($rowno,$rowperpage) {
 
    $this->db->select('zone_donnees,type_donnees,count(id_donnees) as nbre');
    $this->db->from('donnees');	
	$this->db->group_by(array('zone_donnees','type_donnees'));
    $this->db->limit($rowperpage, $rowno); 
    $query = $this->db->get();
 
    return $query->result();
  }

  // Select total records
  public function getrecordCount() {

    $this->db->select('count(*) as allcount');
    $this->db->from('donnees');	
	$this->db->group_by(array('zone_donnees','type_donnees'));
    $query = $this->db->get();
    $result = $query->result();
 
    return $result[0]->allcount;
  }
}