<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdi_model extends CI_Model {

	public function getRows($params = array(),$tab){ 
        $this->db->select('*'); 
        $this->db->from($tab); 
         
        if(array_key_exists("where", $params)){ 
            foreach($params['where'] as $key => $val){ 
                $this->db->where($key, $val); 
            } 
        } 
         
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){ 
            $result = $this->db->count_all_results(); 
        }else{ 
            if(array_key_exists("id", $params) || (array_key_exists("returnType", $params) && $params['returnType'] == 'single')){ 
                if(!empty($params['id'])){ 
                    $this->db->where('id', $params['id']); 
                } 
                $query = $this->db->get(); 
                $result = $query->row_array(); 
            }else{ 
                $this->db->order_by('id', 'desc'); 
                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){ 
                    $this->db->limit($params['limit'],$params['start']); 
                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){ 
                    $this->db->limit($params['limit']); 
                } 
                 
                $query = $this->db->get(); 
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE; 
            } 
        } 
         
        // Return fetched data 
        return $result; 
    } 

    public function get_all_table_query_t($user)
	{
        //SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_SCHEMA = "codeigniter"
		$this->db->select('TABLE_NAME');
        $this->db->from('information_schema.tables');
        $this->db->where('TABLE_SCHEMA','codeigniter');
        $this->db->like('TABLE_NAME',$user);
		$query = $this->db->get();
		return $result = $query->result();
    }

    public function get_alter_table_query($table,$val1)
	{
        //ALTER TABLE '.$table[0].' ADD '.$val1.' VARCHAR(1000) NOT NULL 
		// $query = $this->db->query("ALTER TABLE $table ADD $val1 VARCHAR(1000) NOT NULL");
		// return $result = $query->result();
	}
	
	public function insert_json_in_db($tab,$json_data)
	{
		$this->db->insert($tab,$json_data);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
	
	
	public function get_count_sondage_query($tab)
	{
		$this->db->select("COUNT(*) as num_row");
		$this->db->from($tab);
		$query = $this->db->get();
		$result = $query->result();
		return $result[0]->num_row;
		
	}
	
	
	public function get_sondage_pagination_query($limit,$start,$tab)
	{
		
		// $this->db->select('*');
		// $this->db->from('donnees');
		$this->db->limit($limit, $start);
		$query = $this->db->get($tab);
		if($query->num_rows() > 0 ) {
			return $query->result_array();
		}else{
			return false;
		}
		
		//echo $this->db->last_query();
		// return $result = $query->result();
	}

    
}
