<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sondage_model extends CI_Model {
	
	public function new_sondage_query($data)
	{
		$this->db->insert('sondage',$data);
		return $insert_id = $this->db->insert_id();
	}
	
	public function get_count_sondage_query()
	{
		$this->db->select("COUNT(*) as num_row");
		$this->db->from('sondage');
		$query = $this->db->get();
		$result = $query->result();
		return $result[0]->num_row;
	}
	
	public function get_sondage_pagination_query($limit,$start)
	{
		$this->db->select('*');
		$this->db->from('sondage');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $result = $query->result();
	}
	
	public function get_all_sondage_query()
	{
		$this->db->select('*');
		$this->db->from('sondage');
		$query = $this->db->get();
		return $result = $query->result();
		//SELECT * FROM sondage
	}
	
	public function get_sondage_by_Id_query($sond_id)
	{
		$this->db->select('*');
		$this->db->from('sondage');
		$this->db->where('id_sondage',$sond_id);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $result = $query->row();
	}
	
	public function update_sondage_query($sondage_id,$data)
	{
		$this->db->where('id_sondage',$sondage_id);
		return $this->db->update('sondage',$data);
	}
}