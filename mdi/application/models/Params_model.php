<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Params_model extends CI_Model {
	
	public function new_projet_query($data)
	{
		$this->db->insert('projets',$data);
		return $insert_id = $this->db->insert_id();
	}
	
	public function new_occupation_query($data)
	{
		$this->db->insert('occupation',$data);
		return $insert_id = $this->db->insert_id();
	}
	
	public function new_zone_query($data)
	{
		$this->db->insert('zone',$data);
		return $insert_id = $this->db->insert_id();
	}
	
	public function get_count_projet_query()
	{
		$this->db->select("COUNT(*) as num_row");
		$this->db->from('projets');
		$query = $this->db->get();
		$result = $query->result();
		return $result[0]->num_row;
	}
	
	public function get_sondage_pagination_query($limit,$start)
	{
		
		$this->db->select('zone_donnees,type_donnees,count(id_donnees) as nbre');
		$this->db->from('donnees');
		$this->db->limit($limit, $start);
		$this->db->group_by(array('zone_donnees','type_donnees','id_donnees'));
		$query = $this->db->get('');
		if($query->num_rows() > 0 ) {
			return $query->result();
		}else{
			return false;
		}
		
		//echo $this->db->last_query();
		// return $result = $query->result();
	}
	
	public function get_count_data_query()
	{
		$this->db->select("zone_donnees,type_donnees,COUNT(*) as num_row");
		$this->db->from('donnees');
		$this->db->group_by(array('zone_donnees','type_donnees'));
		$query = $this->db->get();
		$result = $query->result();
		return $result[0]->num_row;
	}
	
	public function get_all_projets_query()
	{
		$this->db->select('*');
		$this->db->from('projets');
		$query = $this->db->get();
		return $result = $query->result();
		//SELECT * FROM sondage
	}
	
	public function get_all_occupations_query()
	{
		$this->db->select('*');
		$this->db->from('occupations');
		$query = $this->db->get();
		return $result = $query->result();
		//SELECT * FROM sondage
	}
	
	public function get_all_zones_query()
	{
		$this->db->select('*');
		$this->db->from('zones');
		$query = $this->db->get();
		return $result = $query->result();
		//SELECT * FROM sondage
	}
	
	public function get_projet_by_Id_query($proj_id)
	{
		$this->db->select('*');
		$this->db->from('projets');
		$this->db->where('id_projet',$proj_id);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $result = $query->row();
	}
	
	public function update_projets_query($proj_id,$data)
	{
		$this->db->where('id_projet',$proj_id);
		return $this->db->update('projets',$data);
	}
	
	// Fetch records
  public function getData($rowno,$rowperpage) {
 
    $this->db->select('zone_donnees,type_donnees,count(id_donnees) as nbre');
    $this->db->from('donnees');	
	$this->db->group_by(array('zone_donnees','type_donnees'));
    $this->db->limit($rowperpage, $rowno); 
    $query = $this->db->get();
 
    return $query->result();
  }


  // Select total records
  public function getrecordCount() {

    // $this->db->select('count(*) as allcount');
    // $this->db->from('donnees');	
	// $this->db->group_by(array('zone_donnees','type_donnees'));
    // $query = $this->db->get();
	
    $query = $this->db->query("SELECT count(*)  as allcount
	FROM (SELECT zone_donnees,type_donnees
	FROM donnees
	GROUP BY zone_donnees,type_donnees) Tablereponse");
    $result = $query->result();
 
    return $result[0]->allcount;
  }
}