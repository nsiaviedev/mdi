<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdi_model extends CI_Model {

    public function get_all_table_query($user)
	{
        //SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_SCHEMA = "codeigniter"
		$this->db->select('TABLE_NAME');
        $this->db->from('information_schema.tables');
        $this->db->where('TABLE_SCHEMA','codeigniter');
        $this->db->like('TABLE_NAME',$user);
		$query = $this->db->get();
		return $result = $query->result();
    }

    public function get_alter_table_query($table,$val1)
	{
        //ALTER TABLE '.$table[0].' ADD '.$val1.' VARCHAR(1000) NOT NULL 
		// $query = $this->db->query("ALTER TABLE $table ADD $val1 VARCHAR(1000) NOT NULL");
		// return $result = $query->result();
    }
	
	public function get_count_sondage_querys()
	{
		$this->db->select("COUNT(*) as num_row");
		$this->db->from('Remise_en_vigueurEloukouWilliamsTampe1580739815');
		$query = $this->db->get();
		$result = $query->result();
		return $result[0]->num_row;
		
	}
	
	public function get_count_sondage_query($tab)
	{
		$this->db->select("COUNT(*) as num_row");
		$this->db->from($tab);
		$query = $this->db->get();
		$result = $query->result();
		return $result[0]->num_row;
		
	}
	
	public function get_sondage_pagination_querys($limit,$start)
	{
		
		// $this->db->select('*');
		// $this->db->from('donnees');
		$this->db->limit($limit, $start);
		$query = $this->db->get('Remise_en_vigueurEloukouWilliamsTampe1580739815');
		if($query->num_rows() > 0 ) {
			return $query->result_array();
		}else{
			return false;
		}
		
		//echo $this->db->last_query();
		// return $result = $query->result();
	}
	
	public function get_sondage_pagination_query($limit,$start,$tab)
	{
		
		// $this->db->select('*');
		// $this->db->from('donnees');
		$this->db->limit($limit, $start);
		$query = $this->db->get($tab);
		if($query->num_rows() > 0 ) {
			return $query->result_array();
		}else{
			return false;
		}
		
		//echo $this->db->last_query();
		// return $result = $query->result();
	}

     public function get_table_from_user_query()
    {
        $query = $this->db->query("SELECT * FROM Remise_en_vigueurEloukouWilliamsTampe1580739815;");
        return $result = $query->result();
    }
    
}
