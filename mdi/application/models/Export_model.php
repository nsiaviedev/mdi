<?php
/**
 * Description of Export Model
 *
 * @author TechArise Team
 *
 * @email  info@techarise.com
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Export_model extends CI_Model {
    // get employee list
    public function employeeLists($user) {
        $this->db->select('*');
        $this->db->from('codeigniter.classeur1tampeeloukouwilliams');
        $this->db->like('TABLE_NAME',$user);
        $query = $this->db->get();
        return $query->result_array();

        
    }

    // get employee list
    public function employeeList() {
        $this->db->select('*');
        $this->db->from('codeigniter.users');
        $query = $this->db->get();
        return $query->result_array();

        
    }
}
?>