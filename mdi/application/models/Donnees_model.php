<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donnees_model extends CI_Model {
	
	public function __construct() {
		parent::__construct(); 
	  }
	
	public function new_donnees_query($data)
	{
		$this->db->insert('donnees',$data);
		return $insert_id = $this->db->insert_id();
	}
	
	public function get_count_sondage_query()
	{
		$this->db->select("COUNT(*) as num_row");
		$this->db->from('donnees');
		$query = $this->db->get();
		$result = $query->result();
		return $result[0]->num_row;
		
	}
	
	public function get_count_search($key)
	{
		$this->db->select("COUNT(*) as num_row");
		$this->db->from('donnees');
		$this->db->like('type_donnees',$key);
		$query = $this->db->get();
		$result = $query->result();
		return $result[0]->num_row;
		
	}
	
	
	public function search($key)
	{
		$this->db->select("*");
		$this->db->from('donnees');
		$this->db->like('type_donnees',$key);
		$query = $this->db->get();
		return $result = $query->result();
		
	}
	
	public function get_data_search_pagination_query($key, $limit,$start)
	{
		
		// $this->db->select('*');
		// $this->db->from('donnees');
		$this->db->from('donnees');
		$this->db->like('type_donnees',$key);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0 ) {
			return $query->result();
		}else{
			return false;
		}
		
		
		//echo $this->db->last_query();
		// return $query->result();
	}
	
	
	
	public function get_sondage_pagination_query($limit,$start)
	{
		
		// $this->db->select('*');
		// $this->db->from('donnees');
		$this->db->limit($limit, $start);
		$query = $this->db->get('donnees');
		if($query->num_rows() > 0 ) {
			return $query->result();
		}else{
			return false;
		}
		
		//echo $this->db->last_query();
		// return $result = $query->result();
	}
	
	public function get_all_donnees_query()
	{
		$this->db->select('*');
		$this->db->from('donnees');
		$query = $this->db->get();
		return $result = $query->result();
		//SELECT * FROM sondage
		
		
	}
	
	public function get_sondage_by_Id_query($sond_id)
	{
		$this->db->select('*');
		$this->db->from('donnees');
		$this->db->where('id_donnees',$sond_id);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $result = $query->row();
	}
	
	public function update_sondage_query($sondage_id,$data)
	{
		$this->db->where('id_donnees',$sondage_id);
		return $this->db->update('donnees',$data);
	}
	
	public function delete_data_query($sondage_id,$data)
	{
		$this->db->where('id_donnees',$sondage_id);
		return $this->db->delete('donnees',$data);
	}
	
	// Fetch records
  public function getData($rowno,$rowperpage,$search="") {
 
    $this->db->select('*');
    $this->db->from('donnees');

    if($search != ''){
      $this->db->like('type_donnees', $search);
      $this->db->or_like('residence_donnees', $search);
    }
	

    $this->db->limit($rowperpage, $rowno); 
    $query = $this->db->get();
 
    return $query->result();
  }

  // Select total records
  public function getrecordCount($search = '') {

    $this->db->select('count(*) as allcount');
    $this->db->from('donnees');
 
    if($search != ''){
      $this->db->like('type_donnees', $search);
      $this->db->or_like('residence_donnees', $search);
    }
	
	

    $query = $this->db->get();
    $result = $query->result();
 
    return $result[0]->allcount;
  }
  
}