<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

  public function __construct(){
 
    parent::__construct();
    $this->load->helper('url');

    // Load session
    $this->load->library('session');

    // Load Pagination library
    $this->load->library('pagination');

    // Load model
    $this->load->model('Main_model');
  }

  public function index(){
    redirect('User/loadRecord');
  }

  public function loadRecord($rowno=0){

    // Search text
    $search_text = "";
    if($this->input->post('submit') != NULL ){
      $search_text = $this->input->post('search');
      $this->session->set_userdata(array("search"=>$search_text));
    }else{
      if($this->session->userdata('search') != NULL){
        $search_text = $this->session->userdata('search');
      }
    }

    // Row per page
    $rowperpage = 100;

    // Row position
    if($rowno != 0){
      $rowno = ($rowno-1) * $rowperpage;
    }
 
    // All records count
    $allcount = $this->Main_model->getrecordCount($search_text);

    // Get records
    $users_record = $this->Main_model->getData($rowno,$rowperpage,$search_text);
 
    // Pagination Configuration
    $config['base_url'] = base_url().'index.php/User/loadRecord';
    $config['use_page_numbers'] = TRUE;
    $config['total_rows'] = $allcount;
    $config['per_page'] = $rowperpage;
	
	$config['uri_segment'] = 3;      
	$config['full_tag_open'] = '<ul class="pagination">';        
	$config['full_tag_close'] = '</ul>';  
	$config['attributes'] = array('class' => 'page-link');   
	$config['first_link'] = 'First';        
	$config['last_link'] = 'Last';   
	$config['first_tag_open'] = '<li>';        
	$config['first_tag_close'] = '</li>';        
	$config['prev_link'] = '&laquo';        
	$config['prev_tag_open'] = '<li class="prev">';        
	$config['prev_tag_close'] = '</li>';        
	$config['next_link'] = '&raquo';        
	$config['next_tag_open'] = '<li>';        
	$config['next_tag_close'] = '</li>';        
	$config['last_tag_open'] = '<li>';        
	$config['last_tag_close'] = '</li>';        
	$config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';        
	$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';        
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	$config['num_links'] = 1;
			
    // Initialize
    $this->pagination->initialize($config);
 
    $data['pagination'] = $this->pagination->create_links();
    $data['result'] = $users_record;
    $data['row'] = $rowno;
    $data['search'] = $search_text;

    // Load view
    $this->load->view('user_view',$data);
 
  }

}