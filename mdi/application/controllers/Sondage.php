<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sondage extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		//connection to model sondage
		$this->load->model('Sondage_model', 'emp_model', TRUE);
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->library('pagination');
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	public function index($data = [])
	{
		if (!$this->ion_auth->logged_in())
		{
			$url_actuellement = uri_string() ;
			$this->session->set_userdata('last_url', $url_actuellement);
			redirect('auth/login');

		}else{

			//var_dump($_SESSION) ;

			// $user = $this->ion_auth->user()->row();

			// $data['user'] = $user;
			// $data['titre'] = "BIENVENUE DANS VOTRE OUTIL FINANCIER - Joseph & Daniel - Advisory" ;

			// $module = $this->uri->segment(2) ;

			// $data['active'] = $module ;
			
			
			$this->load->view('template/header',$data);
			$data['sondages'] = $this->emp_model->get_all_sondage_query();
			// $config['base_url'] = base_url('sondage/index/');        
			// $config['total_rows'] = $this->emp_model->get_count_sondage_query();      
			// $config['per_page'] = 2;        
			// $config['uri_segment'] = 3;        
			// $config['full_tag_open'] = '<ul class="pagination">';        
			// $config['full_tag_close'] = '</ul>';  
			// $config['attributes'] = array('class' => 'page-link');   
			// $config['first_link'] = 'First';        
			// $config['last_link'] = 'Last';   
			// $config['first_tag_open'] = '<li>';        
			// $config['first_tag_close'] = '</li>';        
			// $config['prev_link'] = '&laquo';        
			// $config['prev_tag_open'] = '<li class="prev">';        
			// $config['prev_tag_close'] = '</li>';        
			// $config['next_link'] = '&raquo';        
			// $config['next_tag_open'] = '<li>';        
			// $config['next_tag_close'] = '</li>';        
			// $config['last_tag_open'] = '<li>';        
			// $config['last_tag_close'] = '</li>';        
			// $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';        
			// $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';        
			// $config['num_tag_open'] = '<li>';
			// $config['num_tag_close'] = '</li>';

			// $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			// $this->pagination->initialize($config);        
			// $data['pagination'] = $this->pagination->create_links();     
			//$data['sondages'] = $this->emp_model->get_sondage_pagination_query($config["per_page"], $page);  
			$this->load->view('template/menu',$data);
			$this->load->view('home_sondage',$data);
			
			$data['url'] = $this->uri->segment(1) ;
			$this->load->view('template/footer', $data);
		}
		
		
	}
	
	public function add_new($data = [])
	{
		if (!$this->ion_auth->logged_in())
		{
			$url_actuellement = uri_string() ;
			$this->session->set_userdata('last_url', $url_actuellement);
			redirect('auth/login');

		}else{
			
			$this->load->view('template/header',$data);
			$this->load->view('template/menu',$data);
			$this->load->view('add_new_sondage',$data);
			$this->load->view('template/footer',$data);
		}
	}
	
	public function save()
	{
		if($this->input->post('btn_save')){
			$input_data = $this->input->post('input_val');
			//input_data['emp_pass'] = md5($this->input->post('emp_pass'));
			$is_inserted = $this->emp_model->new_sondage_query($input_data);
			if($is_inserted)
				$this->session->set_flashdata('success','New Sondage has been inserted successfully.'); 
			else
				$this->session->set_flashdata('error','You could not insert new sondage...!');
		}else{
			echo "No request...";
		}
		redirect(base_url('sondage/add_new'));
	}
	
	public function edit($id = false)
	{
		$this->load->view('template/header');
		if(is_numeric($id))
			$sond_id = $id;
		else
			redirect(base_url('sondage/'));
		$data['sondage'] = $this->emp_model->get_sondage_by_Id_query($sond_id);
		$this->load->view('edit_sondage', $data);
		$this->load->view('template/footer');
	}
	
	public function view($id = false)
	{
		$this->load->view('template/header');
		if(is_numeric($id))
			$sond_id = $id;
		else
			redirect(base_url('sondage/'));
		$data['sondage'] = $this->emp_model->get_sondage_by_Id_query($sond_id);
		$this->load->view('edit_sondage', $data);
		$this->load->view('template/footer');
	}
	
	public function update()
	{
		$sondage_id = $this->input->post('sondage_id');
		if($this->input->post('btn_save')){
			$input_data = $this->input->post('input_val');
			$is_updated = $this->emp_model->update_sondage_query($sondage_id, $input_data);
			if($is_updated)
				$this->session->set_flashdata('success','Sondage '.$sondage_id.' update successfully.'); 
			else
				$this->session->set_flashdata('error','You could not update sondage...!');
		}else{
			$this->session->set_flashdata('error','No request...!');
		}
		redirect(base_url().'sondage/edit/'.$sondage_id);
	}
}
