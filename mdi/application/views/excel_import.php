
<!doctype html>
<html lang="en" dir="ltr">
	
<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="msapplication-TileColor" content="#0061da">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />


		<!-- Title -->
		<title>Plight – Admin Bootstrap4 Responsive Webapp Dashboard Template</title>
		<link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>/fonts/fonts/font-awesome.min.css">

		<!-- Font Family -->
		<link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

		<!-- Sidemenu Css -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/toggle-sidebar/sidemenu.css" rel="stylesheet" />


		<!-- Dashboard Css -->
		<link href="<?php echo base_url().'assets'; ?>/css/dashboard.css" rel="stylesheet" />

		<!-- c3.js Charts Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

		<!-- select2 Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/select2/select2.min.css" rel="stylesheet" />

		<!-- Time picker Plugin -->
		<!--<link href="<?php //echo base_url().'assets/'; ?>/plugins/time-picker/jquery.timepicker.css" rel="stylesheet" />-->

		<!-- Date Picker Plugin -->
		<link href="<?php echo base_url().'assets'; ?>/plugins/date-picker/spectrum.css" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<!--<link href="<?php //echo base_url().'assets/'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />-->

		<!-- file Uploads -->
        <link href="<?php echo base_url().'assets'; ?>/plugins/fileuploads/css/dropify.css" rel="stylesheet" type="text/css" />
		<!---Font icons-->
		<link href="<?php echo base_url().'assets'; ?>/plugins/iconfonts/plugin.css" rel="stylesheet" />
		
		<script type="text/javascript"> src="<?php echo base_url(); ?>asset/jquery.min.js"></script>

		<style>
			.success {
				background: #c7efd9;
				border: #bbe2cd 1px solid;
			}

			.error {
				background: #fbcfcf;
				border: #f3c6c7 1px solid;
			}
			.test {
				//border: 1px solid red;
				position: relative;
				top: -8px
			}
		</style>
	</head>
	<body class="app sidebar-mini rtl">
		<div id="/global-loader/" ></div>
		<div class="page">
			<div class="page-main">
				<?php include('template/header.php'); ?>

				<!-- Sidebar menu-->
				<?php include('template/nav.php'); ?>

				<div class="app-content  my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">MDI</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">MDI</a></li>
								<li class="breadcrumb-item active" aria-current="page">Charger un fichier</li>
							</ol>

						</div>

						<div class="row">
							<div class="col-md-12">

								<form class="card" action="" method="post" name="frmCSVImport" id="frmCSVImport" enctype="multipart/form-data">
									<div class="card-header">
										<h3 class="card-title">File Upload</h3>
									</div>

									<span id="success_message"></span>

									<div class="card-body">
										<div class="row">
											<div class="col-md-6">
												<input type="file" name="file" id="file" accept=".xls, .xlsx, .csv" class="dropify" data-height="110" />
												<span id="upload_error"></span>
											</div>

											

											<div class="col-md-6">
												
												<div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>">
													<?php 
														if(!empty($message)) { echo $message; }
													?>
													<?php if($this->session->has_userdata('success')){ ?>
														<div class="alert alert-primary" role="alert">
														<?php echo $this->session->flashdata('success'); ?>
														</div>
													<?php } ?>
													<?php if($this->session->has_userdata('error')){ ?>
														<div class="alert alert-danger" role="alert">
														<?php echo $this->session->flashdata('error'); ?>
														</div>
													<?php } ?>
												</div>
											</div>


										</div>
										
									</div>

									<div class="card-body">
										<div class="row">
											<div class="col-md-6 col-lg-6">
												<div class="form-group m-0">
													<label class="form-label"></label>
													<div class="row gutters-xs">

														<div class="col-auto">
															<label class="colorinput">
																<input name="demandes1[]" type="checkbox"  value="num_der_quittance" class="colorinput-input" />
																<span class="colorinput-color bg-azure"></span>
																<span class="custom-switch-description test">Numéro de la police</span>
															</label>
														</div>
														<div class="col-auto">
															<label class="colorinput">
																<input name="demandes2[]" type="checkbox"  value="num_der_quittance" class="colorinput-input" />
																<span class="colorinput-color bg-azure"></span>
																<span class="custom-switch-description test">Nom apporteur</span>
															</label>
														</div>

														<div class="col-auto">
															<label class="colorinput">
																<input name="demandes3[]" type="checkbox"  value="num_der_quittance" class="colorinput-input" />
																<span class="colorinput-color bg-azure"></span>
																<span class="custom-switch-description test">Code apporteur</span>
															</label>
														</div>
														<div class="col-auto">
															<label class="colorinput">
																<input name="demandes4[]" type="checkbox"  value="num_der_quittance" class="colorinput-input" />
																<span class="colorinput-color bg-azure"></span>
																<span class="custom-switch-description test">Nom du souscription</span>
															</label>
														</div>

														<div class="col-auto">
															<label class="colorinput">
																<input name="demandes5[]" type="checkbox"  value="num_der_quittance" class="colorinput-input" />
																<span class="colorinput-color bg-azure"></span>
																<span class="custom-switch-description test">Contact du souscripteur</span>
															</label>
														</div>

														<div class="col-auto">
															<label class="colorinput">
																<input name="input_val[quit]" type="checkbox"  value="num_der_quittance" class="colorinput-input" />
																<span class="colorinput-color bg-azure"></span>
																<span class="custom-switch-description test">N° dernière quittance émise</span>
															</label>
														</div>
														<div class="col-auto">
															<label class="colorinput">
																<input name="input_val[mont]" type="checkbox" value="mont_der_quittance" class="colorinput-input"  />
																<span class="colorinput-color bg-indigo"></span>
																<span class="custom-switch-description test">Montant dernière quittance émise</span>
																
															</label>
														</div>
														<div class="col-auto">
															<label class="colorinput">
																<input name="input_val[debperiode]" type="checkbox" value="deb_per_couv" class="colorinput-input" />
																<span class="colorinput-color bg-purple"></span>
																<span class="custom-switch-description test">Début période couverture DQE</span>
															</label>
														</div>
														<div class="col-auto">
															<label class="colorinput">
																<input name="input_val[finperiode]" type="checkbox" value="fin_per_couv" class="colorinput-input" />
																<span class="colorinput-color bg-pink"></span>
																<span class="custom-switch-description test">Fin période couverture DQE</span>
															</label>
														</div>
														
													</div>
												</div>
											</div>
											<div class="col-md-6 col-lg-6">
												<div class="form-group m-0">
													<div class="form-label"></div>
													<div class="row gutters-xs">
														<div class="col-auto">
															<label class="colorinput">
																<input name="input_val[index]" type="checkbox" value="index_dqe" class="colorinput-input" />
																<span class="colorinput-color bg-red"></span>
																<span class="custom-switch-description test">Index DQE</span>
															</label>
														</div>
														<div class="col-auto">
															<label class="colorinput">
																<input name="input_val[fineffetquit]" type="checkbox" value="dte_fin_eff" class="colorinput-input" />
																<span class="colorinput-color bg-orange"></span>
																<span class="custom-switch-description test">Date fin effet police</span>
															</label>
														</div>
														<div class="col-auto">
															<label class="colorinput">
																<input name="input_val[etatpolice]" type="checkbox" value="etat_police" class="colorinput-input" />
																<span class="colorinput-color bg-yellow"></span>
																<span class="custom-switch-description test">Etat de la police</span>
															</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card-body">
										<div class="form-footer">
											<input type="submit" id="submit" name="import" value="Charger" class="btn btn-primary btn-block">
										</div>
									</div>
								</form>

								<div class="table-responsive" id="customer_data">

								</div>

								<div class="form-group" id="process" style="display: none;">
										<div class="progress">
											<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valumax="100"></div>
										</div>
									</div>

							</div>
							
						</div>

						<!-- Tableau de la requette -->

<?php if(isset($_POST[""])) {


	if (isset($_POST['demandes1']) && isset($_POST['demandes2']) && empty($_POST['demandes3']) && empty ($_POST['demandes4']) && isset($_POST['demandes5'])){ 
	    echo "demande 125";
	    if ($inf = $_POST ['demandes1'] && $inf = $_POST ['demandes2'] && $inf = $_POST ['demandes5'] ){
	        $req = $bdd1->prepare("SELECT x.contact_souscripteur,b_civie.A.numero_police, B.nom_apporteur 
	                                FROM b_civie.$table[0] A 
	                                LEFT JOIN sunshine.police z ON z.numero_police = b_civie.A.numero_police 
	                                LEFT JOIN sunshine.souscripteur x ON x.numero_souscripteur = z.numero_souscripteur 
	                                LEFT JOIN sunshine.apporteur B ON B.code_apporteur = z.code_apporteur");
	        $donnees = $req->execute();
	        // recuperer des resultats
	        $information = $req -> fetchAll(); 
	?>
	    <div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Full color variations</h3>
					</div>
					<div class="table-responsive">
						<table class="table card-table table-vcenter text-nowrap table-primary" >
							<thead  class="bg-primary text-white">
								<tr >
									<th class="text-white">Police</th>
									<th class="text-white">Nom Apporteur</th>
									<th class="text-white">Contact Apporteur</th>
								</tr>
							</thead>
							<tbody>
								<?php 
			                    foreach ($information as $informa){
			                    ?>
			                    <tr> 
			                        <td><?php echo $informa['numero_police'];?></td>
			                        <td><?php echo $informa['nom_apporteur'];?></td>
			                        <td><?php echo $informa['contact_souscripteur'];?></td>
			                    </tr>
			                    <?php                     
			                    }
			                   ?>
							</tbody>
						</table>
					</div>
					<!-- table-responsive -->
				</div>
			</div>
		</div>    
	<?php
	    }
	}

}
?>


					</div>
				</div>
			</div>

			

			<!--footer-->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
							Copyright © 2018 <a href="#">Plight</a>. Designed by <a href="#">Spruko</a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->
		</div>

		<!-- Back to top -->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>
		<!-- Dashboard Css -->
		<!--<script src="<?php //echo base_url().'assets'; ?>/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="<?php //echo base_url().'assets'; ?>/js/vendors/bootstrap.bundle.min.js"></script>
		<script src="<?php //echo base_url().'assets'; ?>/js/vendors/jquery.sparkline.min.js"></script>
		<script src="<?php //echo base_url().'assets'; ?>/js/vendors/selectize.min.js"></script>
		<script src="<?php //echo base_url().'assets'; ?>/js/vendors/jquery.tablesorter.min.js"></script>
		<script src="<?php //echo base_url().'assets'; ?>/js/vendors/circle-progress.min.js"></script>
		<script src="<?php //echo base_url().'assets'; ?>/plugins/rating/jquery.rating-stars.js"></script>-->

		<!-- Fullside-menu Js-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/toggle-sidebar/sidemenu.js"></script>


		<!--Select2 js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/select2/select2.full.min.js"></script>

		<!-- Timepicker js -->
		<!--<script src="<?php //echo base_url().'assets'; ?>/plugins/time-picker/jquery.timepicker.js"></script>-->
		<script src="<?php echo base_url().'assets'; ?>/plugins/time-picker/toggles.min.js"></script>

		<!-- Datepicker js -->
		<script src="<?php echo base_url().'assets'; ?>/plugins/date-picker/spectrum.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/date-picker/jquery-ui.js"></script>
		<script src="<?php echo base_url().'assets'; ?>/plugins/input-mask/jquery.maskedinput.js"></script>

		<!-- Inline js -->
		<script src="<?php echo base_url().'assets'; ?>/js/select2.js"></script>
		<!-- file uploads js -->
        <script src="<?php echo base_url().'assets'; ?>/plugins/fileuploads/js/dropify.js"></script>

		<!-- Custom scroll bar Js-->
		<!--<script src="<?php //echo base_url().'assets'; ?>/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>-->

		<!-- Custom Js-->
		<!--<script src="<?php //echo base_url().'assets'; ?>/js/custom.js"></script>-->

		<script>

			$(document).ready(function(){

			 load_data();

			 function load_data()
			 {
			  $.ajax({
			   url:"<?php echo base_url(); ?>excel_import/fetch",
			   method:"POST",
			   success:function(data){
			    $('#customer_data').html(data);
			   }
			  })
			 }

			 $('#frmCSVImport').on('submit', function(event){
			  event.preventDefault();
			  $.ajax({
			   url:"<?php echo base_url(); ?>excel_import/import",
			   method:"POST",
			   data:new FormData(this),
			   contentType:false,
			   cache:false,
			   processData:false,
			   success:function(data){
			    $('#file').val('');
			    load_data();
			    alert(data);
			   }
			  })
			 });

			});

		</script>

	</body>
</html>


